/**
 * This file is part of ORB-SLAM2.
 *
 * Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
 * For more information see <https://github.com/raulmur/ORB_SLAM2>
 *
 * ORB-SLAM2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ORB-SLAM2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
 */
#include<iostream>
#include<algorithm>
#include<fstream>
#include<System.h>
#include"Sensor.h"
using namespace std;
using namespace ORB_SLAM2;

// define output video resolution
#define VIDEO_WIDTH     1280
#define VIDEO_HEIGHT    720
#define VIDEO_DELAY     400


std::mutex orbmutex;
std::mutex clickedmutex;


void NetworkThread(Sensor *sensor, bool &shouldQuit) {

    double i = 0;
    sensor->initTime = ClockGetTime();
    while(!shouldQuit) {

        IMUData dataimu;
        dataimu.timestamp = i*100 + 5;
        dataimu.v_x = 0.9;
        dataimu.v_y = -0.1*i;
        dataimu.v_z = 0.2*i;
        sensor->pushData(dataimu);
        i++;
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

int main(int argc, char **argv)
{

    if(argc != 3)
    {
        cerr << endl << "Usage: ./mono_tum path_to_vocabulary path_to_settings" << endl;
        return 1;
    }

    Sensor sensor;
    bool shouldQuit = false;

    // ROTATION TEST
    sensor_t gD;
    gD << 1000, 20, 30, 40;
    sensor.SetRotationMatrix(gD);
    cout << "ROTATION MATRIX:\n" << sensor.refRot << "\n";

    sensor_t tmp;
    tmp(0) = 1000;
    Eigen::Vector3d camCenter;
    camCenter << (double)1,
                 (double)2,
                 (double)3;
    cout << "CAM CENTER: " << camCenter << "\n";
    tmp.tail(3) = sensor.refRot*camCenter;
    cout << "Resulting point" << tmp << std::endl;

    std::thread nt;
    nt = std::thread(NetworkThread, &sensor, std::ref(shouldQuit));

    std::thread integratorThread;
    integratorThread = std::thread(&Sensor::integrate, &sensor);
    cv::VideoCapture cap;
    // open the default camera, use something different from 0 otherwise;
    if(!cap.open(1)) {
        cout << "Could not connect to camera." << endl;
        return 0;
    }
    cap.set(CV_CAP_PROP_FRAME_WIDTH, 1280);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT, 720);
    long long mBeginTime = (long long)ClockGetTime();
    cout << "Starting Time: " << mBeginTime << " ms" << endl;

    // Create SLAM system. It initializes all system threads and gets ready to process frames.
    ORB_SLAM2::System SLAM(argv[1], argv[2], &sensor, true);


    // Vector for tracking time statistics
    cout << endl << "-------" << endl;
    cout << "Start processing sequence ..." << endl;

    // Main loop
    cv::Mat frame;
    int prevState;

    while(!SLAM.isShutdownRequested()) {
        prevState = SLAM.GetTrackingState();
        cap >> frame;
        if (frame.empty()) { continue;}
        double currTime = ClockGetTime() - VIDEO_DELAY - sensor.initTime;

        if(!SLAM.isPaused()) {
            // Pass the image to the SLAM system
            SLAM.TrackMonocular(frame, currTime);

            int currState = SLAM.GetTrackingState();

            if (currState == Tracking::OK) {
                 if(prevState == Tracking::NOT_INITIALIZED) {
                    cout << "Initializing Time: " << (long long)currTime << endl;
                }

            }
            else if (currState == Tracking::LOST) {
            }
            prevState = currState;
        }
        else {
            cv::imshow("Cam", frame);
        }
    }
    shouldQuit = true;

    sensor.shutdownRequested = true;
    integratorThread.join();
    // Stop all threads
    SLAM.Shutdown();
    return 0;
}
