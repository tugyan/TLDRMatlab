/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/
#include<iostream>
#include<algorithm>
#include<fstream>
#include<System.h>
#include"Sensor.h"
#include<TCPServer.h>
#include"engine.h"
//Kubra
#include "GPSConvert.h"
#include "cstdlib"
#include <string>
// #include<math.h>

using namespace std;
using namespace ORB_SLAM2;

// define output video resolution
#define VIDEO_WIDTH     1280
#define VIDEO_HEIGHT    720
#define VIDEO_DELAY     400

asio::io_service io_service;
tcp_server* server;

std::mutex orbmutex;
std::mutex clickedmutex;

geodetic_converter::GeodeticConverter GPSCon;

ORB_SLAM2::System *SLAM;
bool GPSWork = true;
bool connAv = false;

double scale_deg = 0;
double filter_deg = 0;
double mod_deg = 0;
bool isInverse = false;
double isXY = 0;
double isPlot = 0;
void plotThread(Sensor*, bool&);
void gpsThread(Sensor*);
void NetworkThread(Sensor *sns, bool *conAv) {
    try {
        server = new tcp_server(io_service, sns, conAv);
        io_service.run();
        cout << "Network Connection Closed." << endl;
    }
    catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
    }
}

int main(int argc, char **argv)
{

    if(argc != 10)
    {
        cerr << endl << "Usage: ./mono_tum path_to_vocabulary path_to_settings scale_degree mod_degree filter_degree cap_open inverse_pose is_xy is_plot" << endl;
        return 1;
    }

    Sensor sensor;

    // for (size_t i = 0; i < 30; ++i) {
    // std::array<double, 4> data =  {i*2, 0, 0, 0};
    // sensor.IMUPathBuffer.push_back(data);
    //
    // int cevab = sensor.findTimeIMUPath(-10);
    // cout << "El cevab: " << cevab << std::endl;
    // cout << "El cevab değer: " << sensor.IMUPathBuffer[cevab][0] << std::endl;
    bool shouldQuit = false;
    std::thread matlabThread;

    matlabThread = std::thread(plotThread, &sensor, std::ref(shouldQuit));

    //Kubra
    scale_deg = std::stod(std::string(argv[3]));
    mod_deg = std::stod(std::string(argv[4]));
    filter_deg = std::stod(std::string(argv[5]));
    if (std::string(argv[7]) == "inverse"){
        //std::cout << "INVERSE SET" << std::endl;
        isInverse = true;
    }
    if (std::string(argv[8]) == "xy"){
        isXY = 1;
    }
    if (std::string(argv[9]) == "plot"){
        isPlot = 1;
    }

    std::thread nT(NetworkThread, &sensor, &connAv);
    std::thread integratorThread;
    std::thread GPSthread;

    networkPopup(connAv);
    if(!connAv) {
        io_service.stop();
        nT.join();
    }
    else {
        integratorThread = std::thread(&Sensor::integrate, &sensor);
        GPSthread = std::thread(gpsThread, &sensor);
    }
    cv::VideoCapture cap;
    // open the default camera, use something different from 0 otherwise;
    if(!cap.open(std::stod(std::string(argv[6])))) {
        cout << "Could not connect to camera." << endl;
        return 0;
    }
    cap.set(CV_CAP_PROP_FRAME_WIDTH, 1280);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT, 720);
    long long mBeginTime = (long long)ClockGetTime();
    cout << "Starting Time: " << mBeginTime << " ms" << endl;

    // Create SLAM system. It initializes all system threads and gets ready to process frames.
    SLAM = new System(argv[1], argv[2], &sensor, true);

    // Vector for tracking time statistics
    cout << endl << "-------" << endl;
    cout << "Start processing sequence ..." << endl;

    // Main loop
    cv::Mat frame;
    int prevState;
    int frameCounter = 0;
    int currState = 0;
    double avgTrack = 0.0;
    double currTime = 0;
    double ttrack = 0;
    unsigned int loopN = 0;

    while(!SLAM->isShutdownRequested()) {
        prevState = SLAM->GetTrackingState();
        cap >> frame;
        if (frame.empty()) { continue;}
        currTime = ClockGetTime() - VIDEO_DELAY - sensor.initTime;
        if(!SLAM->isPaused()) {
            std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
            // Pass the image to the SLAM system
            SLAM->TrackMonocular(frame, currTime);
            std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
            ttrack= std::chrono::duration_cast<std::chrono::duration<double> >(t2 - t1).count();
            avgTrack += ttrack;
            loopN += 1;

            currState = SLAM->GetTrackingState();
            if (currState == Tracking::OK) {
                if(prevState == Tracking::NOT_INITIALIZED) {
                    cout << "Initializing Time: " << (long long)currTime << endl;
                    sensor.SetORBInitialized(currTime);
                }
                frameCounter++;
            }
            else if (currState == Tracking::LOST) {
                frameCounter = 0;
            }
            prevState = currState;
        }
        else {
            cv::imshow("Cam", frame);
        }
    }
    avgTrack /= loopN;
    cout << "Average Track time CPU: " << avgTrack << endl;
    shouldQuit = true;
    matlabThread.join();
    GPSthread.join();
    if(connAv) {
        sensor.shutdownRequested = true;
        integratorThread.join();
        io_service.stop();
        nT.join();
    }

    SLAM->Shutdown();
    // Save camera trajectory
    //SLAM.SaveTrajectoryTUM("AllFrameTrajectory" + std::to_string(mBeginTime) + ".txt");
    //SLAM.SaveKeyFrameTrajectoryTUM("KeyFrameTrajectory" + std::to_string(mBeginTime) + ".txt");
    return 0;
}


//----------------------------GPS THREAD---------------------------------------------------------------------------------
void gpsThread(Sensor *sensor) {
    sensor->convertGPS();
    GPSWork = false;
    cout << "!!!!!!!!!!!GPS THREAD CLOSED!!!!!!!!!!" <<endl;
}


//------------------------------MATLAB THREAD------------------------------------------------------------------------------
void plotThread(Sensor *sensor, bool &shouldQuit) {
    cout << "Trying to open MATLAB engine." << endl;
    Engine *ep = engOpen(NULL);		// Connect to MATLAB engine
    cout << "Matlab engine initiated." << endl;

    //MATLAB string
    string matlab_str;
    matlab_str = "cd ~/; "
    "if(isPlot),"
    "   f = figure(1); set(f,'Position',[0 0 800 600]);"
    "   axis equal; title('ESTIMATED vs GPS TRAJECTORY'); grid on;"
    "   xlabel('x-axis(north) in meter'); ylabel('y-axis(east) in meter'); zlabel('z-axis(up) in meter');"
    "   ORB = line('parent', gca, 'color', 'b', 'linewidth', 2, 'xdata', [], 'ydata', [], 'zdata', []);"
    "   GPS = line('parent', gca, 'color', 'r', 'linewidth', 2, 'xdata', [], 'ydata', [], 'zdata', []);"
    "   TARGET = line('parent', gca, 'linestyle', 'none', 'Marker', '*', 'MarkerSize', 10, 'color', 'k', 'xdata', [], 'ydata', [], 'zdata', []);"
    "   START = line('parent', gca, 'linestyle', 'none', 'Marker', '+', 'MarkerSize', 8, 'color', 'm', 'xdata', [], 'ydata', [], 'zdata', []);"
    "   ENDD = line('parent', gca, 'linestyle', 'none', 'Marker', 'o', 'MarkerSize', 8, 'color', 'c', 'xdata', [], 'ydata', [], 'zdata', []);"
    "   legend('ORB', 'GPS', 'TARGET');"
    "   gps_time = inf*ones(10000,1); gps_size = 0;"
    "   gps_data = zeros(10000,3);"
    "   init_gps = [39.8720295 32.7501075 0];"
    "   first_gps = 1;"
    "   spheroid = referenceEllipsoid('wgs84', 'm');"
    "   target = [0 0 0];"
    "end;"
    "imu_time = inf*ones(10000, 1); imu_size = 0;"
    "imu_data = zeros(10000,3);"
    "R2 = eye(3); R1 = [0, 0, 1; 1, 0, 0; 0, 1, 0];";


    string imu_str =
    "imu_time(imu_size+1:imu_size+buffer_size) = buffer_time(1:buffer_size);"
    "imu_data(imu_size+1:imu_size+buffer_size,1) = BUFFERx(1:buffer_size);"
    "imu_data(imu_size+1:imu_size+buffer_size,2) = BUFFERy(1:buffer_size);"
    "imu_data(imu_size+1:imu_size+buffer_size,3) = BUFFERz(1:buffer_size);"
    "imu_size = imu_size+buffer_size;";

    string kf_str =
    "kf_time = kf_time(1:kf_size);"
    "kf_pos = zeros(kf_size,3);"
    "kf_pos(:,1) = KFx(1:kf_size);"
    "kf_pos(:,2) = KFy(1:kf_size);"
    "kf_pos(:,3) = KFz(1:kf_size);"
    "kf_pos = kf_pos * R1' * R2';";

    string all_str =
    "all_time = all_time(1:all_size);"
    "all_pos = zeros(all_size,3);"
    "all_pos(:,1) = ALLx(1:all_size);"
    "all_pos(:,2) = ALLy(1:all_size);"
    "all_pos(:,3) = ALLz(1:all_size);"
    "all_pos = medfilt1(all_pos,filter_deg);"
    "all_pos = all_pos * R1' * R2';"
    "if(imu_size > 0),"
    "   first_imu = sum( imu_time(1:imu_size) <= kf_time(1) );"
    "   imu_data = imu_data - imu_data(first_imu,:);"
    "   if(isPlot & gps_size >0 ),"
    "       first_gps = sum( gps_time(1:gps_size) <= kf_time(1));"
    "   end;"
    "end;";

    string cont_scale_str1 =
    "last_kf_ind = sum(kf_time <= imu_time(imu_size));"
    "   last_kf_time = kf_time(last_kf_ind);"
    "   Ns = fix( (last_kf_ind-1)/scale_deg );"
    "   scaleXYZ = ones(Ns, 3);"//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    "   scale_time = zeros(1,Ns);"
    "   ks = 1;"
    "   for i = 1+scale_deg:scale_deg:last_kf_ind,"
    "       scale_ind1 = sum(kf_time(i-scale_deg) >= imu_time(1:imu_size));"
    "       scale_ind2 = sum(kf_time(i) >= imu_time(1:imu_size));"
    "       scale_time(ks) = kf_time(i-scale_deg);"
    //--------------------------SCALE XYZ--------------------------------------------------
    "       imu_distX = sqrt(sum((imu_data(scale_ind2,1) - imu_data(scale_ind1,1)).^2));"
    "       orb_distX = sqrt(sum((kf_pos(i,1) - kf_pos(i-scale_deg,1)).^2));"
    "       imu_distY = sqrt(sum((imu_data(scale_ind2,2) - imu_data(scale_ind1,2)).^2));"
    "       orb_distY = sqrt(sum((kf_pos(i,2) - kf_pos(i-scale_deg,2)).^2));"

    "       imu_distXY = sqrt(sum((imu_data(scale_ind2,1:2) - imu_data(scale_ind1,1:2)).^2));"
    "       orb_distXY = sqrt(sum((kf_pos(i,1:2) - kf_pos(i-scale_deg,1:2)).^2));"
    //-------------------------- Z DEGREE----------------------------------------------
    "       if(rem(ks, mod_deg) == 1 || mod_deg == 1),"
    "           imu_distZ = sqrt(sum((imu_data(scale_ind2,3) - imu_data(scale_ind1,3)).^2));"
    "           orb_distZ = sqrt(sum((kf_pos(i,3) - kf_pos(i-scale_deg,3)).^2));"
    "       end;"

    //-----------------------------SCALE RESULTS-------------------------------
    "       if(isXY),"
    "           scaleXYZ(ks,:) = [imu_distXY/orb_distXY, imu_distXY/orb_distXY, imu_distZ/orb_distZ];"
    "       else,"
    "           scaleXYZ(ks,:) = [imu_distX/orb_distX, imu_distY/orb_distY, imu_distZ/orb_distZ];"
    "       end;"
    //---------------------------CONTROL WİTH PREVİOUS---------------------------
    "       if(scaleXYZ(ks,1) == 0),"
    "           if(ks == 1),"
    "               scaleXYZ(ks,1) = 1;"
    "           else,"
    "               scaleXYZ(ks,1) = scaleXYZ(ks-1,1);"
    "           end;"
    "       end;"
    "       if(scaleXYZ(ks,2) == 0),"
    "           if(ks == 1),"
    "               scaleXYZ(ks,2) = 1;"
    "           else,"
    "               scaleXYZ(ks,2) = scaleXYZ(ks-1,2);"
    "           end;"
    "       end;"
    "       if(scaleXYZ(ks,3) == 0),"
    "           if(ks == 1),"
    "               scaleXYZ(ks,3) = 1;"
    "           else,"
    "               scaleXYZ(ks,3) = scaleXYZ(ks-1,3);"
    "           end;"
    "       end;"

    "       ks = ks+1;"
    "   end;";




    string cont_scale_str2 =

    //-----------------------------------FILTER---------------------------------
    "   scale_filXYZ = scaleXYZ;"
    "   scale_meanXYZ = mean(scaleXYZ, 1);"
    "   scale_stdXYZ = std(scaleXYZ,1);"
    "   mean_std1 = repmat(scale_meanXYZ + scale_stdXYZ, size(scaleXYZ, 1) ,1);"
    "   mean_std2 = repmat(scale_meanXYZ - scale_stdXYZ, size(scaleXYZ, 1) ,1);"
    "   if(Ns > 10),"
    "       indd = scaleXYZ > (scale_meanXYZ + scale_stdXYZ);"
    "       scale_filXYZ( indd ) = mean_std1(indd);"
    "       indd = scaleXYZ < (scale_meanXYZ - scale_stdXYZ);"
    "       scale_filXYZ( indd ) = mean_std2(indd);"
    "   end;"
    //-------------------------------SCALE TRAJECTORY--------------------------
    "   scaled_allXYZ = scale_filXYZ(1,:) .* all_pos;"
    "   ks = 2;"
    "   for i = 1+scale_deg:scale_deg:last_kf_ind-scale_deg,"
    "       ind = find(all_time == kf_time(i) );"
    "       L = scale_filXYZ(ks,:) ./ scale_filXYZ(ks-1,:);"
    "       scaled_allXYZ(ind:end,:) = scaled_allXYZ(ind:end,:) .* L - scaled_allXYZ(ind,:) .* L + scaled_allXYZ(ind,:);"
    "       ks = ks+1;"
    "   end;"
    "   xxx = scaled_allXYZ(:,1);"
    "   yyy = scaled_allXYZ(:,2);"
    "   zzz = scaled_allXYZ(:,3);";
        string scale_target =
    //-----------------------------------TARGET------------------------
    "   target = target * R1' * R2'; "
    "   for i = 1:target_size,"
    "       if((target_time(i,1) > scale_time(1)) ),"
    "           target_scale = mean(mean(scale_filXYZ((target_time(i,1) < scale_time) & scale_time < target_time(i,2))));"
    "           target(i,:) = target(i,:) .* target_scale;"
    "       end;"
    "   end;"
    "   tx = target(:,1);"
    "   ty = target(:,2);"
    "   tz = target(:,3);"
    "   if(isPlot),"
    "       gps_size = max(gps_size, 1);"
    "       first_gps = max(first_gps, 1);"
    "       set(ORB, 'xdata', xxx + gps_data(first_gps,1), 'ydata', yyy + gps_data(first_gps, 2), 'zdata', -zzz - gps_data(first_gps,3 ));"
    "       set(GPS, 'xdata', gps_data(1:gps_size,1), 'ydata', gps_data(1:gps_size,2), 'zdata', -gps_data(1:gps_size,3) );"
    "       set(TARGET, 'xdata', target(:,1)+ gps_data(first_gps,1), 'ydata', target(:,2) + gps_data(first_gps,2), 'zdata', -target(:,3) - gps_data(first_gps,3) );"
    "       set(START, 'xdata', [gps_data(1,1); scaled_allXYZ(1,1)], 'ydata', [gps_data(1,2); scaled_allXYZ(1,2)], 'zdata', [-gps_data(1,3); -scaled_allXYZ(1,3)]);"
    "       set(ENDD, 'xdata', [gps_data(gps_size,1); scaled_allXYZ(end,1)], 'ydata', [gps_data(gps_size,2); scaled_allXYZ(end,2)], 'zdata', [-gps_data(gps_size,3); -scaled_allXYZ(end,3)]);"
    "   end;";


    string gps_str =
    "gps_time(gps_size+1:gps_size+buffer_size) = buffer_time(1:buffer_size);"
    "gps_data(gps_size+1:gps_size+buffer_size,1) = BUFFERx(1:buffer_size);"
    "gps_data(gps_size+1:gps_size+buffer_size,2) = BUFFERy(1:buffer_size);"
    "gps_data(gps_size+1:gps_size+buffer_size,3) = BUFFERz(1:buffer_size);"
    "gps_size = gps_size+buffer_size;";

//    engEvalString(ep, matlab_str.c_str());

    //MATLAB variables
    //TEMP pointer
    const mwSize TEMP_size1 = 3;   //size of matlab pointer
    const mwSize TEMP_size2 = 1;   //size of matlab pointer
    mxArray *TEMP = mxCreateDoubleMatrix(TEMP_size1, TEMP_size2, mxREAL);  //matlab pointer
    double* tempp = (double *)mxCalloc(TEMP_size1, sizeof(double));

    //TIME pointer
    const mwSize TIME_size1 = 1;   //size of matlab pointer
    const mwSize TIME_size2 = 1;   //size of matlab pointer
    mxArray *TIME = mxCreateDoubleMatrix(TIME_size1, TIME_size2, mxREAL);  //matlab pointer
    double *timecp = (double *)mxCalloc(TIME_size1, sizeof(double));

    //ORB pointer
    const mwSize ORB_size1 = 8000;   //size of matlab pointer
    const mwSize ORB_size2 = 1;   //size of matlab pointer
    mxArray *ORBtime = mxCreateDoubleMatrix(ORB_size1, ORB_size2, mxREAL);  //matlab pointer
    mxArray *ORBx = mxCreateDoubleMatrix(ORB_size1, ORB_size2, mxREAL);  //matlab pointer
    mxArray *ORBy= mxCreateDoubleMatrix(ORB_size1, ORB_size2, mxREAL);  //matlab pointer
    mxArray *ORBz = mxCreateDoubleMatrix(ORB_size1, ORB_size2, mxREAL);  //matlab pointer

    //IMU pointer
    const mwSize IMU_size1 = 2000;   //size of matlab pointer
    const mwSize IMU_size2 = 1;   //size of matlab pointer
    mxArray *IMUx = mxCreateDoubleMatrix(IMU_size1, IMU_size2, mxREAL);  //matlab pointer
    mxArray *IMUy = mxCreateDoubleMatrix(IMU_size1, IMU_size2, mxREAL);  //matlab pointer
    mxArray *IMUz = mxCreateDoubleMatrix(IMU_size1, IMU_size2, mxREAL);  //matlab pointer
    mxArray *IMUtime = mxCreateDoubleMatrix(IMU_size1, IMU_size2, mxREAL);  //matlab pointer

    //TARGET pointer
    const mwSize TARGET_size1 = 10;   //size of matlab pointer
    const mwSize TARGET_size2 = 1;   //size of matlab pointer
    mxArray *TARGETx = mxCreateDoubleMatrix(TARGET_size1, TARGET_size2, mxREAL);  //matlab pointer
    mxArray *TARGETy = mxCreateDoubleMatrix(TARGET_size1, TARGET_size2, mxREAL);  //matlab pointer
    mxArray *TARGETz = mxCreateDoubleMatrix(TARGET_size1, TARGET_size2, mxREAL);  //matlab pointer
    mxArray *TARGETtime1 = mxCreateDoubleMatrix(TARGET_size1, TARGET_size2, mxREAL);  //matlab pointer
    mxArray *TARGETtime2 = mxCreateDoubleMatrix(TARGET_size1, TARGET_size2, mxREAL);  //matlab pointer

    //MATLAB TAKEBACK pointer
    mxArray *CPP = nullptr;


    //C2MATLAB variables
    double orbtime[ORB_size1] = {0};
    double orbx[ORB_size1] = {0};
    double orby[ORB_size1] = {0};
    double orbz[ORB_size1] = {0};

    double imutime[IMU_size1] = {0};
    double imux[IMU_size1] = {0};
    double imuy[IMU_size1] = {0};
    double imuz[IMU_size1] = {0};

    double targetx[TARGET_size1] = {0};
    double targety[TARGET_size1] = {0};
    double targetz[TARGET_size1] = {0};
    double targettime1[TARGET_size1] = {0};
    double targettime2[TARGET_size1] = {0};

    double *cppx = nullptr;
    double *cppy = nullptr;
    double *cppz = nullptr;

    double imu_curr_time = -100;
    int kf_size = 0;
    int all_size = 0;
    int buffer_size = 0;
    int imu_size = 0;
    int IMUBufferSize = 0;
    int target_size = 0;
    size_t CPP_size = 0;

    //OTHER VARAIBLES
    bool tryRotLate = true;
    bool LOOPCLOSE = false;

    int GPSBufferSize = 0;
    int gps_size = 0;
    bool init_gps_sent = false;
    bool isGPSInit;

    //-----------------------paramaters----------------------------------------
    *timecp = (double)scale_deg;
    mxSetPr(TIME, timecp);
    engPutVariable(ep, "scale_deg", TIME);

    *timecp = (double)mod_deg;
    mxSetPr(TIME, timecp);
    engPutVariable(ep, "mod_deg", TIME);

    *timecp = (double)isXY;
    mxSetPr(TIME, timecp);
    engPutVariable(ep, "isXY", TIME);

    *timecp = (double)isPlot;
    mxSetPr(TIME, timecp);
    engPutVariable(ep, "isPlot", TIME);

    *timecp = (double)filter_deg;
    mxSetPr(TIME, timecp);
    engPutVariable(ep, "filter_deg", TIME);

    cout << "SCALE DEGREE: " << scale_deg << endl;
    cout << "MOD DEGREE: " << mod_deg << endl;
    cout << "FILTER DEGREE: " << filter_deg << endl;
    cout << "INVERSE POSE: " << isInverse << endl;
    cout << "SCALE ON XY: " << isXY << endl;
    cout << "MATLAB PLOT: " << isPlot << endl;

    engEvalString(ep, matlab_str.c_str());
    cout << "-------------------------MATLAB READY-------------------------"<<endl;

    sensor_buffer_t sfcORB;
    sensor_buffer_t sfcTarget;
    //LOOP
    while(!shouldQuit) {
        //---------------------------MATLAB IMU SEND--------------------------------------------------------------------
        sensor->IntegrationMutex.lock();
        IMUBufferSize = sensor->IMUPathBuffer.size();
        sensor->IntegrationMutex.unlock();

        buffer_size = 0;
        sensor->IntegrationMutex.lock();
        while(imu_size < IMUBufferSize) {
            imutime[buffer_size] = sensor->IMUPathBuffer[imu_size][0];
            imux[buffer_size] = sensor->IMUPathBuffer[imu_size][1];
            imuy[buffer_size] = sensor->IMUPathBuffer[imu_size][2];
            imuz[buffer_size] = sensor->IMUPathBuffer[imu_size++][3];
            buffer_size++;
        }
        sensor->IntegrationMutex.unlock();
        imu_curr_time = imutime[buffer_size-1];

        if(buffer_size != 0){
            memcpy(mxGetData(IMUtime), imutime, buffer_size*sizeof(imutime)/IMU_size1 );
            memcpy(mxGetData(IMUx), imux,  buffer_size*sizeof(imutime)/IMU_size1 );
            memcpy(mxGetData(IMUy), imuy,  buffer_size*sizeof(imutime)/IMU_size1 );
            memcpy(mxGetData(IMUz), imuz,  buffer_size*sizeof(imutime)/IMU_size1 );
            *timecp = (double)buffer_size;
            mxSetPr(TIME, timecp);

            engPutVariable(ep, "buffer_size", TIME);
            engPutVariable(ep, "buffer_time", IMUtime);
            engPutVariable(ep, "BUFFERx", IMUx);
            engPutVariable(ep, "BUFFERy", IMUy);
            engPutVariable(ep, "BUFFERz", IMUz);
            //cout <<"imu" <<endl;
            engEvalString(ep, imu_str.c_str());
            //cout <<"------imu" <<endl;
        } //end of buffer size check


        //-----------------------------------GPS--------------------------------
        if(isPlot && GPSWork){
            sensor->GPSMutex.lock();
            GPSBufferSize = sensor->GPSPathBuffer.size();
            isGPSInit = sensor->isGPSInitialiased;
            sensor->GPSMutex.unlock();

            buffer_size = 0;
            sensor->GPSMutex.lock();
            while(gps_size < GPSBufferSize) {
                imutime[buffer_size] = sensor->GPSPathBuffer[gps_size][0];
                imux[buffer_size] = sensor->GPSPathBuffer[gps_size][1];
                imuy[buffer_size] = sensor->GPSPathBuffer[gps_size][2];
                imuz[buffer_size] = sensor->GPSPathBuffer[gps_size++][3];
                buffer_size++;
            }//gps buffer if
            sensor->GPSMutex.unlock();

            if(buffer_size != 0){
                memcpy(mxGetData(IMUtime), imutime, buffer_size*sizeof(imutime)/IMU_size1 );
                memcpy(mxGetData(IMUx), imux,  buffer_size*sizeof(imutime)/IMU_size1 );
                memcpy(mxGetData(IMUy), imuy,  buffer_size*sizeof(imutime)/IMU_size1 );
                memcpy(mxGetData(IMUz), imuz,  buffer_size*sizeof(imutime)/IMU_size1 );
                *timecp = (double)buffer_size;
                mxSetPr(TIME, timecp);

                engPutVariable(ep, "buffer_size", TIME);
                engPutVariable(ep, "buffer_time", IMUtime);
                engPutVariable(ep, "BUFFERx", IMUx);
                engPutVariable(ep, "BUFFERy", IMUy);
                engPutVariable(ep, "BUFFERz", IMUz);
                engEvalString(ep, gps_str.c_str());
            } //end of buffer size check


            if( isGPSInit && !init_gps_sent )
            {
                cout << "!!!!!!INITIAL GPS SENT MATLAB!!!!!: " << endl;
                // sensor->GPSCon.getReference(&lat0, &lon0, &alt0);
                tempp[0] = (double)sensor->lat0;
                tempp[1] = (double)sensor->lon0;
                tempp[2] = (double)sensor->alt0;
                cout << "GPS LAT: " << tempp[0]  << ", LONGT: " << tempp[1]  << ", ALTI: " << tempp[2] << endl;
                mxSetPr(TEMP, tempp);
                engPutVariable(ep, "init_gps", TEMP);
                init_gps_sent = true;
            }

        }
        //----------------------------GET UPDATED KF------------------------------
        if (SLAM != nullptr) {
            vector<KeyFrame*> updatedKF = SLAM->mpMap->GetAllKeyFrames();
            //sort data
            if( !updatedKF.empty() && updatedKF.size() > 5 ) {
                sort(updatedKF.begin(),updatedKF.end(),KeyFrame::lId);
                //-------------------------LOOP CLOSE CONTROL
                LOOPCLOSE = SLAM->MapChanged();
                if(LOOPCLOSE && isInverse)
                {
                    cv::Mat Two = updatedKF[0]->GetPoseInverse();
                    for(size_t i=0; i<updatedKF.size(); i++)
                    {
                        KeyFrame* pKF = updatedKF[i];
                        pKF->SetPose(pKF->GetPose()*Two);
                    }
                }
                kf_size = 0;
                for(size_t i = 0; i < updatedKF.size(); i++)
                {
                    KeyFrame* pKF = updatedKF[i];
                    if(pKF->isBad())
                    continue;
                    cv::Mat centerKF = pKF->GetCameraCenter();
                    orbtime[kf_size] = pKF->mTimeStamp;
                    orbx[kf_size] = (double)centerKF.at<float>(0);
                    orby[kf_size] = (double)centerKF.at<float>(1);
                    orbz[kf_size] = (double)centerKF.at<float>(2);
                    kf_size = kf_size + 1;

                }
                memcpy(mxGetData(ORBtime), orbtime, kf_size*sizeof(orbtime)/ORB_size1 );
                memcpy(mxGetData(ORBx), orbx,  kf_size*sizeof(orbtime)/ORB_size1 );
                memcpy(mxGetData(ORBy), orby,  kf_size*sizeof(orbtime)/ORB_size1 );
                memcpy(mxGetData(ORBz), orbz,  kf_size*sizeof(orbtime)/ORB_size1 );
                *timecp = (double)kf_size;
                mxSetPr( TIME, timecp);

                engPutVariable(ep, "kf_size", TIME);
                engPutVariable(ep, "kf_time", ORBtime);
                engPutVariable(ep, "KFx", ORBx);
                engPutVariable(ep, "KFy", ORBy);
                engPutVariable(ep, "KFz", ORBz);

                engEvalString(ep, kf_str.c_str());

                if(connAv){
                    if( tryRotLate && kf_size > 3) {
                        cout << "!!!GIMBAL REQUESTED!!!" <<endl;
                        sensor_t gb = server->requestGimbal(orbtime[0]);
                        sensor->SetRotationMatrix(gb);
                        cout << "GIMBAL YAW: " << gb[1] <<", PITCH: " << gb[2]<< ", ROLL: " << gb[3] <<endl;
                        tempp[0] = (double)gb[1];
                        tempp[1] = (double)gb[2];
                        tempp[2] = (double)gb[3];

                        // use click opint pointer instead create new one
                        mxSetPr(TEMP, tempp);
                        engPutVariable(ep, "angle", TEMP);
                        engEvalString(ep, "R2 = angle2dcm(-angle(1)*pi/180, +angle(2)*pi/180, -angle(3)*pi/180, 'ZXY');");
                        tryRotLate = false;
                    }

                }



                //      ----------------------------GET UPDATED ALL FRAME------------------------------


                list<ORB_SLAM2::KeyFrame*>::iterator lRit = SLAM->mpTracker->mlpReferences.begin();
                list<double>::iterator lT = SLAM->mpTracker->mlFrameTimes.begin();
                list<bool>::iterator lbL = SLAM->mpTracker->mlbLost.begin();
                all_size = 0;
                for(list<cv::Mat>::iterator lit=SLAM->mpTracker->mlRelativeFramePoses.begin(),
                lend=SLAM->mpTracker->mlRelativeFramePoses.end();lit!=lend;lit++, lRit++, lT++, lbL++)
                {
                    if(*lbL)
                    continue;

                    KeyFrame* pKF = *lRit;
                    cv::Mat Trw = cv::Mat::eye(4,4,CV_32F);
                    while(pKF->isBad())
                    {
                        Trw = Trw*pKF->mTcp;
                        pKF = pKF->GetParent();
                    }

                    Trw = Trw*pKF->GetPose();

                    cv::Mat Tcw = (*lit)*Trw;
                    cv::Mat Rwc = Tcw.rowRange(0,3).colRange(0,3).t();
                    cv::Mat twc = -Rwc*Tcw.rowRange(0,3).col(3);

                    //           SEND MATLAB POINTER
                    orbtime[all_size] = *lT;
                    orbx[all_size] = (double)twc.at<float>(0);
                    orby[all_size] = (double)twc.at<float>(1);
                    orbz[all_size] = (double)twc.at<float>(2);
                    all_size = all_size+1;
                }
                memcpy(mxGetData(ORBtime), orbtime, all_size*sizeof(orbtime)/ORB_size1 );
                memcpy(mxGetData(ORBx), orbx,  all_size*sizeof(orbtime)/ORB_size1 );
                memcpy(mxGetData(ORBy), orby,  all_size*sizeof(orbtime)/ORB_size1 );
                memcpy(mxGetData(ORBz), orbz,  all_size*sizeof(orbtime)/ORB_size1 );
                *timecp = (double)all_size;
                mxSetPr( TIME, timecp);

                engPutVariable(ep, "all_size", TIME);
                engPutVariable(ep, "all_time", ORBtime);
                engPutVariable(ep, "ALLx", ORBx);
                engPutVariable(ep, "ALLy", ORBy);
                engPutVariable(ep, "ALLz", ORBz);

                engEvalString(ep, all_str.c_str());

            }//END OF if updatedkf is nonempity

            //------------------------------------CLICKED---------------------------------------------------------
            SLAM->triangulateMutex.lock();
            SLAM->targets.back();
            target_size = 0;
            for(auto t: SLAM->targets){
                if(t->isLocated){
                    targetx[target_size] = t->location(0);
                    targety[target_size] = t->location(1);
                    targetz[target_size] = t->location(2);
                    targettime1[target_size] = t->firstTime;
                    targettime2[target_size] = t->lastTime;
                    target_size++;
                }
            }
            SLAM->triangulateMutex.unlock();

            memcpy(mxGetData(TARGETtime1), targettime1, target_size*sizeof(targettime1)/TARGET_size1 );
            memcpy(mxGetData(TARGETtime2), targettime2, target_size*sizeof(targettime1)/TARGET_size1 );
            memcpy(mxGetData(TARGETx), targetx,  target_size*sizeof(targettime1)/TARGET_size1);
            memcpy(mxGetData(TARGETy), targety,  target_size*sizeof(targettime1)/TARGET_size1);
            memcpy(mxGetData(TARGETz), targetz,  target_size*sizeof(targettime1)/TARGET_size1);
            *timecp = (double)target_size;
            mxSetPr( TIME, timecp);

            engPutVariable(ep, "target_size", TIME);
            engPutVariable(ep, "target_time1", TARGETtime1);
            engPutVariable(ep, "target_time2", TARGETtime2);
            engPutVariable(ep, "TARGETx", TARGETx);
            engPutVariable(ep, "TARGETy", TARGETy);
            engPutVariable(ep, "TARGETz", TARGETz);
            engEvalString(ep, "target = [TARGETx(1:target_size), TARGETy(1:target_size), TARGETz(1:target_size)];");
            engEvalString(ep, "target_time = [target_time1(1:target_size), target_time2(1:target_size)];");

            if(LOOPCLOSE)
            {
                cout << "*************************LOOP CLOSED*****************************"<<endl;
            }
        }//SLAM is not nullpointer



        //------------------------------------------------SCALE-----------------------------------------------------------
        if(imu_size > 10 && kf_size > 3*scale_deg)
        {
                engEvalString(ep, cont_scale_str1.c_str());
                //cout << "aaaaaaaa" << endl ;
                engEvalString(ep, cont_scale_str2.c_str());
                //cout << "bbb" << endl ;
                engEvalString(ep, scale_target.c_str());
                //cout << "ccc" << endl ;

                //-----------------------READ ORB FROM MATLAB------------------------------------------------------------------------
                CPP = engGetVariable(ep, "xxx");
                cppx = mxGetPr(CPP);
                CPP = engGetVariable(ep, "yyy");
                cppy = mxGetPr(CPP);
                CPP = engGetVariable(ep, "zzz");
                cppz = mxGetPr(CPP);
                CPP_size = mxGetM(CPP);

                //-----------------------SEND ORB TO PLOT VIEWER--------------------------------
                sfcORB.clear();
                for (size_t m = 0; m < CPP_size; m++) {
                    sensor_t data;
                    data[0] = orbtime[m];
                    data[1] = cppx[m];
                    data[2] = cppy[m];
                    data[3] = cppz[m];
                    sfcORB.push_back(data);
                }
                sensor->ORBMatlabBuffer = &sfcORB;

                //-----------------------READ TARGET FROM MATLAB------------------------------------------------------------------------
                CPP = engGetVariable(ep, "tx");
                cppx = mxGetPr(CPP);
                CPP = engGetVariable(ep, "ty");
                cppy = mxGetPr(CPP);
                CPP = engGetVariable(ep, "tz");
                cppz = mxGetPr(CPP);
                CPP_size = mxGetM(CPP);

                //-----------------------SEND TARGET TO PLOT VIEWER--------------------------------
                sfcTarget.clear();
                for (size_t m = 0; m < CPP_size; m++) {
                    sensor_t data;
                    data[0] = cppx[m];
                    data[1] = cppy[m];
                    data[2] = cppz[m];
                    sfcTarget.push_back(data);
                }
                sensor->TargetMatlabBuffer = &sfcTarget;
            }

    }//END OF WHILE SHOULD QUIT

    //-----------------------CLOSE THE MATLAB--------------------------------
    std::this_thread::sleep_for(std::chrono::milliseconds(3));

    //MATLAB ENGINEs Close
    mxDestroyArray(IMUtime);
    mxDestroyArray(IMUx);
    mxDestroyArray(IMUy);
    mxDestroyArray(IMUz);
    mxDestroyArray(ORBx);
    mxDestroyArray(ORBx);
    mxDestroyArray(ORBy);
    mxDestroyArray(ORBz);
    mxDestroyArray(TIME);
    mxDestroyArray(TEMP);
    mxDestroyArray(CPP);
    engClose(ep);
}
