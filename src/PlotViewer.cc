#include "PlotViewer.h"
#include "pangolin/gl/glcuda.h"
#include "pangolin/var/var.h"

void DrawStartPoint(Eigen::Vector2d pnt) {
    glColor3f(0, 1, 0);
    pangolin::glDrawCircle(pnt, 0.01);
}

void DrawHeadPoint(Eigen::Vector2d pnt) {
    glColor3f(0, 0, 1);
    glBegin(GL_TRIANGLE_STRIP);
    glVertex2f( pnt[0] - 0.02, pnt[1]);
    glVertex2f( pnt[0], pnt[1] + 0.02);
    glVertex2f( pnt[0], pnt[1] - 0.02);
    glVertex2f( pnt[0] + 0.02, pnt[1]);
    glEnd();
}

namespace ORB_SLAM2 {
PlotViewer::PlotViewer(bool* s, std::mutex *mtx, Sensor* sns, std::vector<Target*>* trgt)
{
    sensor = sns;
    pTargets = trgt;
    this->ViewerMutex = mtx;
    shutDown = s;
}

PlotViewer::PlotViewer(System* sys)
{
    pSystem = sys;
    sensor = sys->pSensor;
    ORBEstimates = &sys->scaledOrbPath;
    pTargets = &sys->targets;
    this->ViewerMutex = &sys->ViewerMutex;
    shutDown = &sys->mpViewer->mbFinishRequested;
}
void PlotViewer::Run()
{

    roi_l = MAP_SIZE_X/2;
    roi_b = MAP_SIZE_Y/2;

    this->ViewerMutex->lock();
    pangolin::CreateWindowAndBind("Trajectory Viewer",900,800);

    // 3D Mouse handler requires depth testing to be enabled
    // glEnable(GL_DEPTH_TEST);
    glEnable( GL_LINE_SMOOTH | GL_BLEND);

    pangolin::CreatePanel("MapPanel").SetBounds(0.2,1.0,0.0,pangolin::Attach::Pix(250));

    //pangolin::Var<std::string> initGPSBox("MapPanel.Initial GPS");

    hdl = new MapHandler();
    pangolin::View& d_image = pangolin::Display("image")
      .SetBounds(0.1, 1.0f, pangolin::Attach::Pix(250) ,1.0f ,1.0f)
      .SetLock(pangolin::LockLeft, pangolin::LockTop)
      .SetHandler(hdl);

    // Optionally add named labels
    std::vector<std::string> labels;
    labels.push_back(std::string("Time (ms)"));
    labels.push_back(std::string("Altitude (m)"));
    altitudeLog.SetLabels(labels);

    // const float tinc = 100.0f;

    pangolin::Plotter plotter(new pangolin::DataLog, 0.0f, 5*60, -10, 50, 1, 5);
    plotter.SetBounds(0.0, 0.2, 0.0, 1.0f);
    plotter.Track("$i");
    plotter.AddSeries("$0/1000", "$1", pangolin::DrawingModeLine, pangolin::Colour::Blue(), "Altitude (m)", &altitudeLog);

    // Add some sample annotations to the plot
    plotter.AddMarker(pangolin::Marker::Vertical,      0, pangolin::Marker::LessThan    , pangolin::Colour::Blue().WithAlpha(0.2f) );
    plotter.AddMarker(pangolin::Marker::Horizontal,  100, pangolin::Marker::GreaterThan , pangolin::Colour::Red().WithAlpha(0.2f) );
    plotter.AddMarker(pangolin::Marker::Horizontal, -100, pangolin::Marker::LessThan    , pangolin::Colour::Red().WithAlpha(0.2f) );

    pangolin::DisplayBase().AddDisplay(plotter);

    // float t = 0;
    this->ViewerMutex->unlock();

    pangolin::Var<std::string> TargetTitle("MapPanel.---TARGETS---");
    pangolin::GlTextureCudaArray imageTexture;
    imageTexture.LoadFromFile("gmap2band.jpg", false);

    while(!pangolin::ShouldQuit())
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Create new targetBoxes and location information if new targets clicked.
        for(size_t i=targetBoxes.size(); i<pTargets->size(); i++){
            pangolin::Var<bool> tmp("MapPanel.Target" + std::to_string(i), true, true );
            targetBoxes.push_back(tmp);
            activeTargets.push_back(true);

            pangolin::Var<std::string> latTmp("MapPanel.Lat" + std::to_string(i) + ": ");
            pangolin::Var<std::string> longTmp("MapPanel.Long" + std::to_string(i) + ": ");
            pangolin::Var<std::string> altTmp("MapPanel.Alt" + std::to_string(i) + ": ");
            std::array<pangolin::Var<std::string>, 3> tmpArr = {{latTmp, longTmp, altTmp}};
            this->posTargets.push_back(tmpArr);
        }

        // if (initGPSBox.GuiChanged()) {
        //     initPoint = StringToGPS(initGPSBox.Get());
        // }
        //display the image
        d_image.Activate();
        glColor3f(1.0,1.0,1.0);

        roi_l += hdl->dx;
        roi_b += hdl->dy;
        hdl->dir_x = 0;
        hdl->dir_y = 0;
        hdl->dx = 0;
        hdl->dy = 0;

        pangolin::Viewport vp(roi_l, roi_b, ROI_SIZE_X*hdl->zoom_factor, ROI_SIZE_Y*hdl->zoom_factor);
        // vp.ActivateAndScissor();
        imageTexture.RenderToViewport(vp, false, false);

        if (sensor->isGPSInitialiased) {
            DrawGPSRoute();
        }
        if (pTargets) {
            DrawTargets();
        }
        DrawEstimateRoute();
        // altitudeLog.Log(t, 20 + 30*sin(t/1000.0f));
        // t += tinc;
        pangolin::FinishFrame();


        if(shutDown != NULL){
            if(*shutDown){
                break;
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}

void PlotViewer::DrawTargets() {
    if(sensor->TargetMatlabBuffer == nullptr) {
        return;
    }
    Eigen::Vector2d initPoint;
    initPoint = PixelToMeters(GPSToPixel((double) sensor->lat0, (double) sensor->lon0));

    size_t idx = 0;
    for (auto pnt: *sensor->TargetMatlabBuffer) {
        if(targetBoxes[idx]){

            Eigen::Vector2d drawLoc = MetersToGL(initPoint[0] + pnt(1), initPoint[1] - pnt(0));
            glColor3f(0, 1, 1);
            pangolin::glDrawCircle(drawLoc, 0.02);
            glColor3f(1, 1, 1);
            pangolin::glDrawCross(drawLoc, 0.04);
            // auto targGPS = GLToGPS(drawLoc(1), drawLoc(0));
            long double tmpLat, tmpLong, tmpAlt;
            sensor->GPSCon.ned2Geodetic(pnt(1), pnt(0), pnt(2), &tmpLat, &tmpLong, &tmpAlt);
            auto tmpArr = this->posTargets[idx];
            // tmpArr[0] = std::to_string(targGPS(0));
            // tmpArr[1] = std::to_string(targGPS(1));
            // tmpArr[2] = std::to_string(pnt(2) + sensor->alt0);
            tmpArr[0] = std::to_string(tmpLat);
            tmpArr[1] = std::to_string(tmpLong);
            tmpArr[2] = std::to_string(tmpAlt);
        }
        idx++;
    }

    // for (size_t i = 0; i < targetBoxes.size(); i++) {
    //     if(targetBoxes[i]){
    //         activeTargets[i] = true;
    //         Target* target = pTargets->at(i);
    //         // target->calculateTargetLocation();
    //         if (target->isLocated) {
    //             // Eigen::Vector2d drawLoc = initPoint + MetersToGL(target->location(1)*target->scale[1],
    //             //                                                 target->location(0)*target->scale[0]);
    //             // pangolin::glDrawCross(drawLoc, 0.05);
    //             // pangolin::glDrawCircle(drawLoc, 0.05);
    //             // pangolin::glDrawCircle(drawLoc, target->meanError/ROI_SIZE_X);
    //         }
    //     }
    //     else {
    //         activeTargets[i] = false;
    //     }
    // }
}

void PlotViewer::DrawGPSRoute() {
    glColor3f(0, 0, 1);
    Eigen::Vector2d vertices;
    glBegin(GL_LINE_STRIP);
    for (auto point: sensor->GPSPathBuffer) {
        vertices = GPSToGL(point[1], point[2]);
        glVertex2f(vertices[0], vertices[1]);
    }
    glEnd();
}

void PlotViewer::DrawEstimateRoute() {

    glLineWidth(3);
    altitudeLog.Clear();
    Eigen::Vector2d initPoint;
    initPoint = PixelToMeters(GPSToPixel((double) sensor->lat0, (double) sensor->lon0));
    Eigen::Vector2d vertices;
    DrawStartPoint(MetersToGL(initPoint[0], initPoint[1]));
    if (sensor->ORBMatlabBuffer == nullptr)
        return;
    glColor3f(1, 0, 0);
    glBegin(GL_LINE_STRIP);

    for (auto point: *(sensor->ORBMatlabBuffer)){
        //std::cout << "Matlab Buffer" << point << std::endl;
        vertices = MetersToGL(initPoint[0] + point[2], initPoint[1] - point[1]);
        glVertex2f(vertices[0], vertices[1]);
        altitudeLog.Log(point[0], -point[3] + sensor->alt0);
    }
    glEnd();
    auto lastPoint = (*sensor->ORBMatlabBuffer).back();
    vertices = MetersToGL(initPoint[0] + lastPoint[2], initPoint[1] - lastPoint[1]);
    // std::cout << vertices << std::endl;
    DrawHeadPoint(vertices);
}


Eigen::Vector2d PlotViewer::GPSToPixel(double latitude, double longitude) const {
    double x = -(MAP_TOP_LEFT_LONGITUDE - longitude)/LONGITUDE_PER_PIXEL;
    double y = (MAP_TOP_LEFT_LATITUDE - latitude)/LATITUDE_PER_PIXEL;
    Eigen::Vector2d pnt;
    pnt << x, y;
    return pnt;
}

Eigen::Vector2d PlotViewer::MetersToPixel(float x, float y) const {
    Eigen::Vector2d pnt;
    pnt << x/METERS_PER_PIXEL, y/METERS_PER_PIXEL;
    return pnt;
}

Eigen::Vector2d PlotViewer::PixelToMeters(Eigen::Vector2d pxs) const {
    Eigen::Vector2d pnt;
    pnt << pxs[0]*METERS_PER_PIXEL, pxs[1]*METERS_PER_PIXEL;
    return pnt;
}

Eigen::Vector2d PlotViewer::GPSToGL(double latitude, double longitude) const {
    Eigen::Vector2d pnt;

    pnt << 2*((longitude - MAP_TOP_LEFT_LONGITUDE)/LONGITUDE_PER_PIXEL - roi_l)/(ROI_SIZE_X*hdl->zoom_factor) - 1,
        2*(MAP_SIZE_Y + ((latitude - MAP_TOP_LEFT_LATITUDE)/LATITUDE_PER_PIXEL - roi_b))/(ROI_SIZE_Y*hdl->zoom_factor) - 1;
    return pnt;
}

Eigen::Vector2d PlotViewer::GLToGPS(double x, double y) const {
    Eigen::Vector2d pnt;

    pnt << LATITUDE_PER_PIXEL*(((y + 1)*ROI_SIZE_Y*hdl->zoom_factor)/2 - MAP_SIZE_Y + roi_b) + MAP_TOP_LEFT_LATITUDE,
           (((x + 1)*(ROI_SIZE_X*hdl->zoom_factor))/2 + roi_l)* LONGITUDE_PER_PIXEL + MAP_TOP_LEFT_LONGITUDE;
    return pnt;
}

Eigen::Vector2d PlotViewer::MetersToGL(float x, float y) const {
    Eigen::Vector2d pnt;
    pnt << 2*(x/METERS_PER_PIXEL - roi_l)/(ROI_SIZE_X*hdl->zoom_factor) - 1,
        2*(MAP_SIZE_Y - (y/METERS_PER_PIXEL + roi_b))/(ROI_SIZE_Y*hdl->zoom_factor) - 1;

    return pnt;
}

Eigen::Vector2d PlotViewer::StringToGPS(std::string gpsStr) const {

    std::stringstream gpstr(gpsStr);
    std::string s;

    std::getline(gpstr, s, ',');
    double initlat = std::stod(s);

    std::getline(gpstr, s);
    double initlong = std::stod(s);
    return GPSToGL(initlat, initlong);
}

}
