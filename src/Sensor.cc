#include"Sensor.h"
#include<thread>



Sensor::Sensor(): currScale(1.0), shutdownRequested(false) {

        coorRot <<  0, 0, 1,
                    1, 0, 0,
                    0, 1, 0;

        refRot <<   1, 0, 0,
                    0, 1, 0,
                    0, 0, 1;
}


int Sensor::FindMatchingDataIMU(double ts) {

    if (this->IMUDataBuffer.empty()) {
        return -1;
    }

    this->SensorMutex.lock();
    IMUData data = this->IMUDataBuffer.front();
    this->SensorMutex.unlock();
    bool sensorSmall = data.timestamp < ts;

    // If first sensor data came later than frame
    // it is either match or we missed the initialization.
    if(!sensorSmall) {
        if (data.timestamp - ts < 300) {
            return 0;
        }
        return -2;
    }
    auto first = std::lower_bound(this->IMUDataBuffer.cbegin(), this->IMUDataBuffer.cend(), ts);

    int timeout = 0;
    while(timeout++ < 5) {

        this->SensorMutex.lock();
        bool lastBigger = (*first) < this->IMUDataBuffer.back().timestamp;
        this->SensorMutex.unlock();

        if (lastBigger) {
            return first - this->IMUDataBuffer.begin();
        }
    }
    return -3;
}


void Sensor::pushData(IMUData data) {
    std::unique_lock<std::mutex> lock(this->SensorMutex);
    this->IMUDataBuffer.push_back(data);
}


void Sensor::integrate() {

    diffIMUx = 0;
    diffIMUy = 0;
    diffIMUz = 0;

    // Wait until buffer starts to fill
    bool isEmpty = true;
    while(isEmpty) {
        this->SensorMutex.lock();
        isEmpty = this->IMUDataBuffer.empty();
        this->SensorMutex.unlock();
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    sensor_t newPoint;
    newPoint << 0,0,0,0;
    this->IntegrationMutex.lock();
    this->IMUPathBuffer.push_back(newPoint);
    this->IntegrationMutex.unlock();

    size_t currIdx = 0;
    while (!shutdownRequested) {
        this->SensorMutex.lock();
        size_t buffSize = this->IMUDataBuffer.size();
        this->SensorMutex.unlock();
        if (currIdx < buffSize-1) {
            IMUData currData = this->IMUDataBuffer[currIdx++];
            IMUData nextData = this->IMUDataBuffer[currIdx];
            double currTime = currData.timestamp;
            double nextTime = nextData.timestamp;
            double dt = (nextTime - currTime)*1e-3;

            sensor_t newPoint;

            newPoint << nextTime,
                diffIMUx += (currData.v_x + nextData.v_x)* dt/2,
                diffIMUy += (currData.v_y + nextData.v_y)* dt/2,
                diffIMUz += (currData.v_z + nextData.v_z)* dt/2;

            this->IntegrationMutex.lock();
            this->IMUPathBuffer.push_back(newPoint);
            this->IntegrationMutex.unlock();
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
}


void Sensor::convertGPS() {
    std::cout << "!!!GPS THREAD RUN!!!" << std::endl;
    bool gpsControl = true;
    // Wait until buffer starts to fill
    long double gpsx, gpsy, gpsz, lat, longt, alt;
    double currTime;
    bool isEmpty = true;
    int count_init = 1001;  //10sn control at the Beginning
    int count_mid = 11;
    while(isEmpty) {
        this->SensorMutex.lock();
        isEmpty = this->IMUDataBuffer.empty();
        this->SensorMutex.unlock();
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    size_t currIdx = 0;
    // bool gps_flag = true;

    // while(gps_flag && count_init > 0 ){

        // count_init--;

        // std::this_thread::sleep_for(std::chrono::milliseconds(100));
        // this->SensorMutex.lock();
        // size_t buffSize = this->IMUDataBuffer.size();
        // this->SensorMutex.unlock();

        // while(currIdx < buffSize && gps_flag) {
            // this->SensorMutex.lock();
            // IMUData initData = this->IMUDataBuffer[currIdx++];
            // this->SensorMutex.unlock();

            // if( (initData.lat > 36 && initData.lat < 42 && initData.longt > 32 && initData.longt < 42) || !gpsControl){

                // std::cout <<  "--------------GPS INITIALIAZED-----------------" << std::endl;
                // std::cout << "LAT: " << initData.lat << ", LONGT: " << initData.longt << ", ALT: " << initData.alt << std::endl;

                // GPSCon.initialiseReference(initData.lat, initData.longt, initData.alt);
                // lat0 = initData.lat;
                // lon0 = initData.longt;
                // alt0 = initData.alt;
                // isGPSInitialiased = true;
                // gps_flag = false;
            // }
        // }

    // }

    while (! (this->isORBInitialized)) {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }

    currIdx = FindMatchingDataIMU(this->initORBTime);

    if (currIdx == IMUDataBuffer.size()) currIdx--;

    std::cout << "GPS idx: " <<  currIdx << std::endl;
    this->SensorMutex.lock();
    IMUData initData = this->IMUDataBuffer[currIdx++];
    this->SensorMutex.unlock();

    if( (initData.lat > 36 && initData.lat < 42 && initData.longt > 32 && initData.longt < 42)){

        std::cout <<  "--------------GPS INITIALIAZED-----------------" << std::endl;
        std::cout << "LAT: " << initData.lat << ", LONGT: " << initData.longt << ", ALT: " << initData.alt << std::endl;

        GPSCon.initialiseReference(initData.lat, initData.longt, initData.alt);
        lat0 = initData.lat;
        lon0 = initData.longt;
        alt0 = initData.alt;
        isGPSInitialiased = true;
    }
    else {
        std::cout << "GPS is not available. Set initial GPS by hand." << std::endl;
        // initData.printData();
        return;
    }


    while ( count_init > 0 && count_mid > 0 && !shutdownRequested ) {
        count_mid = 11;
        this->SensorMutex.lock();
        size_t buffSize = this->IMUDataBuffer.size();
        this->SensorMutex.unlock();

        while(currIdx < buffSize) {
            IMUData currData = this->IMUDataBuffer[currIdx++];
            currTime = currData.timestamp;

            lat = currData.lat;
            longt = currData.longt;
            alt = currData.alt;

            if( ( lat > 36 && lat < 42 && longt > 32 && longt < 42 ) || !gpsControl ){
                GPSCon.geodetic2Ned( lat, longt, alt, &gpsx, &gpsy, &gpsz);
            }
            else{
                count_mid--;
            }


            sensor_t newPoint;
            newPoint << currTime, (double) gpsx, (double) gpsy, (double) gpsz;

            this->GPSMutex.lock();
            this->GPSPathBuffer.push_back(newPoint);
            this->GPSMutex.unlock();
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}

int Sensor::findTimeIMUPath(double ts) {
    int step;
    size_t count, currIdx, firstIdx = 0;

    this->IntegrationMutex.lock();
    count = this->IMUPathBuffer.size();
    this->IntegrationMutex.unlock();

    while (count > 0)
    {
        currIdx = firstIdx;
        step=count/2;
        currIdx += step;

        this->IntegrationMutex.lock();
        double val = this->IMUPathBuffer[currIdx][0];
        this->IntegrationMutex.unlock();

        if (val < ts) {
            firstIdx=++currIdx;
            count -= step + 1;
        }
        else count=step;
    }
    if(firstIdx == 0 && (abs(this->IMUPathBuffer[0][0] - 200 ) > ts))
    return -1;

    if (firstIdx >= this->IMUPathBuffer.size()) {
        return -2;
    }

    return firstIdx;
}

double Sensor::findIMUDistance(double fromTs, double tillTs) {
    if (this->IMUPathBuffer.empty()) {
        std::cout << "IMU Path Buffer is empty." << std::endl;
        return -1.0;
    }

    sensor_t data = this->IMUPathBuffer.front();

    // If first sensor data came later than frame
    // it is either match or we missed the initialization.
    if (data[0] > fromTs) {
        std::cout << "Beginning time can't be smaller than initTime." << fromTs << " " << data[0] << std::endl;
        return -2.0;
    }

    int firstIdx = findTimeIMUPath(fromTs);
    int secondIdx = findTimeIMUPath(tillTs);
    sensor_t firstPoint = this->IMUPathBuffer[firstIdx];
    sensor_t secondPoint = this->IMUPathBuffer[secondIdx];

    return sqrt(pow(secondPoint[1] - firstPoint[1] ,2)
                + pow(secondPoint[2] - firstPoint[2] ,2)
                + pow(secondPoint[3] - firstPoint[3] ,2));
}


double Sensor::calculateScale(ORB_SLAM2::KeyFrame *firstORBPose, ORB_SLAM2::KeyFrame *lastORBPose) {

    double IMUDifference = findIMUDistance(firstORBPose->mTimeStamp, lastORBPose->mTimeStamp);

    cv::Mat firstCenter = firstORBPose->GetCameraCenter();

    cv::Mat lastCenter = lastORBPose->GetCameraCenter();

    double ORBDifference = cv::norm(firstCenter, lastCenter);

    if(IMUDifference/ORBDifference < 0.0001)
    {
        return currScale;
    }
    currScale = IMUDifference/ORBDifference;
    return currScale;
}

void Sensor::SetRotationMatrix(sensor_t gb) {
    this->refRot = angle2dcm(-gb[1]*M_PI/180, gb[2]*M_PI/180, -gb[3]*M_PI/180).transpose()*coorRot;
}
