#include "IMU.h"
#include "Utils.h"

bool IMUData::FillData(std::string strData) {
    std::string s;
    std::stringstream ss;
    ss.str(strData);
    try {
        std::getline(ss, s, ' ');
        this->timestamp = std::stod(s);

        std::getline(ss, s, ' ');
        if (s != "NaN") {
            this->lat = std::stold(s);
            std::getline(ss, s, ' ');
            this->longt = std::stold(s);
            isGPS = true;
        }

        std::getline(ss, s, ' ');
        this->alt = std::stof(s);

        std::getline(ss, s, ' ');
        this->v_x = std::stof(s);

        std::getline(ss, s, ' ');
        this->v_y = std::stof(s);

        std::getline(ss, s);
        this->v_z = std::stof(s);

        if ((this->alt > 500) ||
            (this->v_x < -30) || (this->v_x > 30) ||
            (this->v_y < -30) || (this->v_y > 30) ||
            (this->v_z < -30) || (this->v_z > 30)) {

            this->isValid = false;
        }
        else {
            this->isValid = true;
        }
    } catch (const std::invalid_argument&) {
        std::cerr << "IMU Argument is invalid: " << ss.str() << std::endl;
        this->isValid = false;
    } catch (const std::out_of_range&) {
        std::cerr << "Argument is out of range for a double\n";
        this->isValid = false;
        throw;
    }
    return isValid;
}

void IMUData::printData() {
    std::cout << "IMU Data: " << std::endl
              << "Time: "  << (long long) this->timestamp << std::endl
              << "VelX "   << this->v_x
              << ", VelY " << this->v_y
              << ", VelZ " << this->v_z << std::endl
              << "Lat "    << this->lat
              << ", Long"  << this->longt
              << ", Alt"   << this->alt << std::endl;
}
