#include "TCPConnection.h"

void tcp_connection::start()
{
    // long long currTime = (long long) ClockGetTime();
    std::vector<char> buf(15);
    socket_.receive(asio::buffer(buf));

    std::string data(buf.data() );

    if (data[0] == 'b') {
        data.erase(0, 1);
        double init_t = std::stod(data);

        this->sensor->initTime = init_t;
    }

    auto handler = std::bind(&tcp_connection::handle_read, shared_from_this(), std::placeholders::_1);
    asio::async_read(socket_, input_buffer_, asio::transfer_at_least(1), handler);
}

void tcp_connection::handle_read(const asio::error_code& err) {
    if (!err || err == asio::error::message_size)
    {
        this->receiveMutex.lock();
        std::string data( (std::istreambuf_iterator<char>(&input_buffer_)), std::istreambuf_iterator<char>() );
        std::istringstream f(data);
        std::string s;
        // std::cout << data << std::endl;
        while (std::getline(f, s)) {

            if (s[0] == 'g') {
                s.erase(0, 1);
                std::istringstream t(s);
                std::string a;
                // std::cout << "Sent Gimball at read_handler " << s << std::endl;
                for (size_t i = 0; i < 4; i++) {
                    std::getline(t, a, ' ');
                    gData[i] = std::stod(a);
                }
                gimbalAvailable = true;
            }
            else if(s[0] == 'i') {
            // if(s[0] == 'i') {
                s.erase(0, 1);
                IMUData imu;
                if (!imu.FillData(s)) {
                    this->strIMUBuf = s;
                    isLastPackIMU = true;
                }
                else {
                    this->sensor->pushData(imu);
                    //imu.printData();
                }
            }
            else if (isLastPackIMU) {
                this->strIMUBuf += s;
                IMUData imu;
                if(imu.FillData(this->strIMUBuf)) {
                    this->sensor->pushData(imu);
                    //imu.printData();
                }
            }
            // else {
                // this->strGimBuf += s;
                // GimballData gimbal;
                // if(gimbal.FillData(this->strGimBuf)) {
                    // this->sensor->pushData(gimbal);
                // }
            // }
        }

        // std::cout << gimballBuffer->DataBuffer.size() << std::endl;
        this->receiveMutex.unlock();
        auto handler = std::bind(&tcp_connection::handle_read, shared_from_this(), std::placeholders::_1);
        asio::async_read(socket_, input_buffer_,
            asio::transfer_at_least(1), handler);
    }

    else {
        std::cout << "Error Occured: " << err << std::endl;
        if (err == asio::error::misc_errors::eof) {
            std::cout << "Connection Lost, EOF reached." << std::endl;
            return;
        }
    }
}

sensor_t tcp_connection::requestGimbal(double ts){
    std::vector<unsigned char> tsBytes = intToBytes((unsigned int) ts);
    tsBytes.insert(tsBytes.begin(), 'g');
    tsBytes.push_back('\n');
    this->receiveMutex.lock();
    socket_.send(asio::buffer(tsBytes));
    this->receiveMutex.unlock();

    while(!gimbalAvailable) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        // std::cout << "in loop" << std::endl;
    }
    gimbalAvailable = false;
    return gData;
}
void tcp_connection::handle_write(const asio::error_code& ec)
{
    if (!ec || ec == asio::error::message_size){
        std::cout << "msg sent" << std::endl;
    }
    else {
        std::cout << "Error Occured: " << ec << std::endl;
    }
}

