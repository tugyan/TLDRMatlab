/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "FrameDrawer.h"
#include "Tracking.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include<mutex>

namespace ORB_SLAM2
{

FrameDrawer::FrameDrawer(Map* pMap): matchPoint(), mpMap(pMap)
{
    mState=Tracking::SYSTEM_NOT_READY;
    mIm = cv::Mat(480,640,CV_8UC3, cv::Scalar(0,0,0));
}

cv::Mat FrameDrawer::DrawFrame()
{
    cv::Mat im;
    vector<cv::KeyPoint> vIniKeys; // Initialization: KeyPoints in reference frame
    vector<int> vMatches; // Initialization: correspondeces with reference keypoints
    vector<cv::KeyPoint> vCurrentKeys; // KeyPoints in current frame
    vector<bool> vbVO, vbMap; // Tracked MapPoints in current frame
    int state; // Tracking state

    //Copy variables within scoped mutex
    {
        unique_lock<mutex> lock(mMutex);
        state=mState;
        if(mState==Tracking::SYSTEM_NOT_READY)
            mState=Tracking::NO_IMAGES_YET;

        mIm.copyTo(im);

        if(mState==Tracking::NOT_INITIALIZED)
        {
            vCurrentKeys = mvCurrentKeys;
            vIniKeys = mvIniKeys;
            vMatches = mvIniMatches;
        }
        else if(mState==Tracking::OK)
        {
            vCurrentKeys = mvCurrentKeys;
            vbVO = mvbVO;
            vbMap = mvbMap;
        }
        else if(mState==Tracking::LOST)
        {
            vCurrentKeys = mvCurrentKeys;
        }
    } // destroy scoped mutex -> release mutex

    if(im.channels()<3) //this should be always true
        cvtColor(im,im,CV_GRAY2BGR);

    //Draw
    if(state==Tracking::NOT_INITIALIZED) //INITIALIZING
    {
        for(unsigned int i=0; i<vMatches.size(); i++)
        {
            if(vMatches[i]>=0)
            {
                cv::line(im,vIniKeys[i].pt,vCurrentKeys[vMatches[i]].pt,
                        cv::Scalar(0,255,0));
            }
        }
    }
    else if(state==Tracking::OK) //TRACKING
    {
        mnTrackedVO=0;
        mnTracked=0;
        const float r = 5;
        cv::Point2f pt1,pt2,pt3,pt4,pt5;

        double meanX = 0;
        double meanY = 0;

        for(int i=0;i<N;i++)
        {
            if(vbVO[i] || vbMap[i])
            {
                pt1.x=vCurrentKeys[i].pt.x-r;
                pt1.y=vCurrentKeys[i].pt.y-r;
                pt2.x=vCurrentKeys[i].pt.x+r;
                pt2.y=vCurrentKeys[i].pt.y+r;

                // This is a match to a MapPoint in the map
                if(vbMap[i])
                {
                    meanX += vCurrentKeys[i].pt.x;
                    meanY += vCurrentKeys[i].pt.y;
                    cv::rectangle(im,pt1,pt2,cv::Scalar(0,255,0));
                    cv::circle(im,vCurrentKeys[i].pt,2,cv::Scalar(0,255,0),-1);
                    mnTracked++;
                }
                else // This is match to a "visual odometry" MapPoint created in the last frame
                {
                    cv::rectangle(im,pt1,pt2,cv::Scalar(255,0,0));
                    cv::circle(im,vCurrentKeys[i].pt,2,cv::Scalar(255,0,0),-1);
                    mnTrackedVO++;
                }
            }
        }
        meanX /= mnTracked;
        meanY /= mnTracked;

        const cv::Point2f CENTER(640, 400);
        const double ARROWLENGTH = 100;

        const cv::Point2f NORTH(640, 400+ARROWLENGTH);
        const cv::Point2f SOUTH(640, 400-ARROWLENGTH);
        const cv::Point2f EAST(640+ARROWLENGTH, 400);
        const cv::Point2f WEST(640-ARROWLENGTH, 400);

        const cv::Point2f NORTHEAST(640+ARROWLENGTH/sqrt(2), 400-ARROWLENGTH/sqrt(2));
        const cv::Point2f NORTHWEST(640-ARROWLENGTH/sqrt(2), 400-ARROWLENGTH/sqrt(2));
        const cv::Point2f SOUTHEAST(640+ARROWLENGTH/sqrt(2), 400+ARROWLENGTH/sqrt(2));
        const cv::Point2f SOUTHWEST(640-ARROWLENGTH/sqrt(2), 400+ARROWLENGTH/sqrt(2));

        // Arrow from mean of features to Video center
        // 1280 x 800
        cv::Point2f arrowEndPoint(meanX, meanY);
        cv::Mat arrowIm;
        im.copyTo(arrowIm);

        const double TOL = 150;
        const double ARROWWIDTH = 15;
        const double OUTERWIDTH = ARROWWIDTH + 5;

        if((meanX>CENTER.x+TOL) & (meanY>CENTER.y+TOL)) {
            cv::arrowedLine(arrowIm, NORTHWEST, CENTER, cv::Scalar(50,50,0), OUTERWIDTH, 8, 0, 0.5);
            cv::arrowedLine(arrowIm, NORTHWEST, CENTER, cv::Scalar(255,255,0), ARROWWIDTH, 8, 0, 0.5);
        }
        else if((meanX<CENTER.x-TOL) & (meanY>CENTER.y+TOL)) {
            cv::arrowedLine(arrowIm, NORTHEAST, CENTER, cv::Scalar(50,50,0), OUTERWIDTH, 8, 0, 0.5);
            cv::arrowedLine(arrowIm, NORTHEAST, CENTER, cv::Scalar(255,255,0), ARROWWIDTH, 8, 0, 0.5);
        }

        else if(meanX>CENTER.x+TOL & meanY<CENTER.y-TOL) {
            cv::arrowedLine(arrowIm, SOUTHWEST, CENTER, cv::Scalar(50,50,0), OUTERWIDTH, 8, 0, 0.5);
            cv::arrowedLine(arrowIm, SOUTHWEST, CENTER, cv::Scalar(255,255,0), ARROWWIDTH, 8, 0, 0.5);
        }
        else if(meanX<CENTER.x-TOL & meanY<CENTER.y-TOL) {
            cv::arrowedLine(arrowIm, SOUTHEAST, CENTER, cv::Scalar(50,50,0), OUTERWIDTH);
            cv::arrowedLine(arrowIm, SOUTHEAST, CENTER, cv::Scalar(255,255,0), ARROWWIDTH, 8, 0, 0.5);
        }
        else if(meanX>CENTER.x+TOL) {
            cv::arrowedLine(arrowIm, WEST, CENTER, cv::Scalar(50,50,0), OUTERWIDTH, 8, 0, 0.5);
            cv::arrowedLine(arrowIm, WEST, CENTER, cv::Scalar(255,255,0), ARROWWIDTH, 8, 0, 0.5);
        }
        else if(meanX<CENTER.x-TOL) {
            cv::arrowedLine(arrowIm, EAST, CENTER, cv::Scalar(50,50,0), OUTERWIDTH, 8, 0, 0.5);
            cv::arrowedLine(arrowIm, EAST, CENTER, cv::Scalar(255,255,0), ARROWWIDTH, 8, 0, 0.5);
        }
        else if(meanY>CENTER.y+TOL) {
            cv::arrowedLine(arrowIm, SOUTH, CENTER, cv::Scalar(50,50,0), OUTERWIDTH, 8, 0, 0.5);
            cv::arrowedLine(arrowIm, SOUTH, CENTER, cv::Scalar(255,255,0), ARROWWIDTH, 8, 0, 0.5);
        }
        else if(meanY<CENTER.y-TOL) {
            cv::arrowedLine(arrowIm, NORTH, CENTER, cv::Scalar(50,50,0), OUTERWIDTH, 8, 0, 0.5);
            cv::arrowedLine(arrowIm, NORTH, CENTER, cv::Scalar(255,255,0), ARROWWIDTH, 8, 0, 0.5);
        }

        cv::addWeighted(arrowIm, 0.5, im, 0.5, 0.0, im);

        cv::rectangle(im,pt1,pt2,cv::Scalar(0,0,255));
        if(areWeTracking){
            pt1.x = pointThatWeTrack.x;
            pt1.y = pointThatWeTrack.y;
            pt2.x = pointThatWeTrack.x-r;
            pt2.y = pointThatWeTrack.y;
            pt3.x = pointThatWeTrack.x+r;
            pt3.y = pointThatWeTrack.y;
            pt4.x = pointThatWeTrack.x;
            pt4.y = pointThatWeTrack.y-r;
            pt5.x = pointThatWeTrack.x;
            pt5.y = pointThatWeTrack.y+r;
            cv::circle(im,pt1,10,cv::Scalar(0,0,255), 3);
            cv::line(im,pt2,pt3,cv::Scalar(0,0,255), 3);
            cv::line(im,pt4,pt5,cv::Scalar(0,0,255), 3);
        }
    }

    cv::Mat imWithInfo;
    DrawTextInfo(im,state, imWithInfo);

    return imWithInfo;
}


void FrameDrawer::DrawTextInfo(cv::Mat &im, int nState, cv::Mat &imText)
{
    stringstream s;
    if(nState==Tracking::NO_IMAGES_YET)
        s << " WAITING FOR IMAGES";
    else if(nState==Tracking::NOT_INITIALIZED)
        s << " TRYING TO INITIALIZE ";
    else if(nState==Tracking::OK)
    {
        if(!mbOnlyTracking)
            s << "SLAM MODE |  ";
        else
            s << "LOCALIZATION | ";
        int nKFs = mpMap->KeyFramesInMap();
        int nMPs = mpMap->MapPointsInMap();
        s << "KFs: " << nKFs << ", MPs: " << nMPs << ", Matches: " << mnTracked;
        if(mnTrackedVO>0)
            s << ", + VO matches: " << mnTrackedVO;
        if(!matchPoint.empty()) {
            s << ", " << matchPoint;
        }
    }
    else if(nState==Tracking::LOST)
    {
        s << " TRACK LOST. TRYING TO RELOCALIZE ";
    }
    else if(nState==Tracking::SYSTEM_NOT_READY)
    {
        s << " LOADING ORB VOCABULARY. PLEASE WAIT...";
    }

    int baseline=0;
    cv::Size textSize = cv::getTextSize(s.str(),cv::FONT_HERSHEY_PLAIN,1,1,&baseline);

    imText = cv::Mat(im.rows+textSize.height+10,im.cols,im.type());
    im.copyTo(imText.rowRange(0,im.rows).colRange(0,im.cols));
    imText.rowRange(im.rows,imText.rows) = cv::Mat::zeros(textSize.height+10,im.cols,im.type());
    cv::putText(imText,s.str(),cv::Point(5,imText.rows-5),cv::FONT_HERSHEY_PLAIN,1,cv::Scalar(255,255,255),1,8);

}

void FrameDrawer::Update(Tracking *pTracker)
{
    unique_lock<mutex> lock(mMutex);
    pTracker->mImGray.copyTo(mIm);
    mvCurrentKeys=pTracker->mCurrentFrame.mvKeys;
    N = mvCurrentKeys.size();
    mvbVO = vector<bool>(N,false);
    mvbMap = vector<bool>(N,false);
    mbOnlyTracking = pTracker->mbOnlyTracking;


    if(pTracker->mLastProcessedState==Tracking::NOT_INITIALIZED)
    {
        mvIniKeys=pTracker->mInitialFrame.mvKeys;
        mvIniMatches=pTracker->mvIniMatches;
    }
    else if(pTracker->mLastProcessedState==Tracking::OK)
    {
        for(int i=0;i<N;i++)
        {
            MapPoint* pMP = pTracker->mCurrentFrame.mvpMapPoints[i];
            if(pMP)
            {
                if(!pTracker->mCurrentFrame.mvbOutlier[i])
                {
                    if(pMP->Observations()>0)
                        mvbMap[i]=true;
                    else
                        mvbVO[i]=true;
                }
            }
        }
    }
    mState=static_cast<int>(pTracker->mLastProcessedState);
}

} //namespace ORB_SLAM
