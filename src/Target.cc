#include <Target.h>
#include <Eigen/SVD>

using namespace ORB_SLAM2;

void Target::addTriangulatePoint(Eigen::Vector2f p, KeyFrame* kf){
	triangulatePixels.push_back(p);
	triangulateKeyFrames.push_back(kf);
}

void Target::calculateTargetLocation(){

    // DO NOT USE LAST N KEY FRAMES
    const int LAST_N = 1;

    size_t numViews = 0;
    Eigen::Matrix<float, Eigen::Dynamic, 4> A(triangulatePixels.size()*2, 4);
	bool firstOneIsFound = false;
    for(int i = 0; i < (int)triangulateKeyFrames.size() - LAST_N; i++){
        auto kf = triangulateKeyFrames[i];
        if(!kf->isBad()) {
			if(!firstOneIsFound){
				firstOneIsFound = true;
				firstTime = kf->mTimeStamp;
			}
			lastTime = kf->mTimeStamp;
            numViews++;
            auto kfPose = kf->GetPose();
            Eigen::Map<Eigen::Matrix<float, 3, 4, Eigen::RowMajor>> P(
                    kfPose.ptr<float>(),
                    kfPose.rows - 1, kfPose.cols);

            P = K*P;
            A.block<2, 4>((numViews - 1)*2, 0) = triangulatePixels[i]*P.block<1, 4>(2, 0) - P.block<2, 4>(0, 0);
        }
    }
    if (numViews > 1) {
        Eigen::BDCSVD<Eigen::Matrix<float, Eigen::Dynamic, 4>> _svd(A.block(0, 0, numViews*2, 4), Eigen::ComputeFullV);
        auto V = _svd.matrixV();
        auto X = V.rightCols<1>();
        X = X/X(X.size() - 1);
        location = X.head(3);
        isLocated = true;
    }
    else{
        isLocated = false;
    }

    // Calculate Error
    if(isLocated){
        Eigen::Vector4f locationH;
        locationH << location, 1;

        meanError = 0;
        for(size_t i = 0; i < triangulateKeyFrames.size() - LAST_N; i++){
            auto kf = triangulateKeyFrames[i];
            if(!kf->isBad()) {
                auto kfPose = kf->GetPose();
                Eigen::Map<Eigen::Matrix<float, 4, 4, Eigen::RowMajor>> P(
                        kfPose.ptr<float>(),
                        kfPose.rows, kfPose.cols);
                Eigen::Vector3f proj2d;
                proj2d = P.block<3,4>(0,0) * locationH;

                // std::cout << "P\n" <<P.block<3,4>(0,0) << std::endl;
                // std::cout << "Proj 2d: \n" << proj2d << std::endl;
                meanError += sqrt( pow(triangulatePixels[i](0) - proj2d(0) / proj2d(2), 2) +
                                   pow(triangulatePixels[i](1) - proj2d(1) / proj2d(2), 2) ) / numViews;
                // std::cout << meanError << std::endl;
            }
        }
    }
}
