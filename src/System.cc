/**
 * This file is part of ORB-SLAM2.
 *
 * Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
 * For more information see <https://github.com/raulmur/ORB_SLAM2>
 *
 * ORB-SLAM2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ORB-SLAM2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
 */


#include "System.h"
#include "Converter.h"
#include <thread>
#include <pangolin/pangolin.h>
#include <iomanip>

namespace ORB_SLAM2
{

    System::System(const string &strVocFile, const string &strSettingsFile, Sensor* sensor, const bool bUseViewer):
        mSensor(MONOCULAR), mpViewer(static_cast<Viewer*>(NULL)), mbReset(false),mbActivateLocalizationMode(false),
        mbDeactivateLocalizationMode(false), frameSaving(false), isRecording(false), shutdownRequest(false)
    {
        // Output welcome message
        cout << endl <<
            "ORB-SLAM2 Copyright (C) 2014-2016 Raul Mur-Artal, University of Zaragoza." << endl <<
            "This program comes with ABSOLUTELY NO WARRANTY;" << endl  <<
            "This is free software, and you are welcome to redistribute it" << endl <<
            "under certain conditions. See LICENSE.txt." << endl << endl;


        //Check settings file
        cv::FileStorage fsSettings(strSettingsFile.c_str(), cv::FileStorage::READ);
        if(!fsSettings.isOpened())
        {
            cerr << "Failed to open settings file at: " << strSettingsFile << endl;
            exit(-1);
        }
        pSensor = sensor;

        float fx = fsSettings["Camera.fx"];
        float fy = fsSettings["Camera.fy"];
        float cx = fsSettings["Camera.cx"];
        float cy = fsSettings["Camera.cy"];

        // Set Intrinsic matrix K
        K << fx, 0, cx,
             0, fy, cy,
             0,  0,  1;

        Kcv = cv::Mat::eye(3,3,CV_32F);
        Kcv.at<float>(0,0) = fx;
        Kcv.at<float>(1,1) = fy;
        Kcv.at<float>(0,2) = cx;
        Kcv.at<float>(1,2) = cy;
        resultPoint = {{0, 0, 0}};

        distCoef = *new cv::Mat(4,1,CV_32F);
        distCoef.at<float>(0) = fsSettings["Camera.k1"];
        distCoef.at<float>(1) = fsSettings["Camera.k2"];
        distCoef.at<float>(2) = fsSettings["Camera.p1"];
        distCoef.at<float>(3) = fsSettings["Camera.p2"];

        //Load ORB Vocabulary
        cout << endl << "Loading ORB Vocabulary." << endl;

        mpVocabulary = new ORBVocabulary();

        bool bVocLoad = mpVocabulary->loadFromBinFile(strVocFile);

        if(!bVocLoad)
        {
            cerr << "Cannot find binary file for vocabulary. " << endl;
            cerr << "Failed to open " << strVocFile << endl;
            cerr << "Trying to open the text file. " << endl;
            bool bVocLoad2 = mpVocabulary->loadFromTextFile(strVocFile + ".txt");
            if(!bVocLoad2)
            {
                cerr << "Wrong path to vocabulary. " << endl;
                cerr << "Falied to open " << strVocFile << ".txt" << endl;
                exit(-1);
            }
            cerr << "Saving the vocabulary to binary for the next time to " << strVocFile << endl;
            mpVocabulary->saveToBinFile(strVocFile);
        }
        cout << "Vocabulary loaded!" << endl << endl;

        //Create KeyFrame Database
        mpKeyFrameDatabase = new KeyFrameDatabase(*mpVocabulary);

        //Create the Map
        mpMap = new Map();

        //Create Drawers. These are used by the Viewer
        mpFrameDrawer = new FrameDrawer(mpMap);
        mpMapDrawer = new MapDrawer(mpMap, strSettingsFile);

        //Initialize the Tracking thread
        //(it will live in the main thread of execution, the one that called this constructor)
        mpTracker = new Tracking(this, mpVocabulary, mpFrameDrawer, mpMapDrawer,
                mpMap, mpKeyFrameDatabase, strSettingsFile, mSensor);

        //Initialize the Local Mapping thread and launch
        mpLocalMapper = new LocalMapping(mpMap, mSensor==MONOCULAR);
        mptLocalMapping = new thread(&ORB_SLAM2::LocalMapping::Run,mpLocalMapper);

        //Initialize the Loop Closing thread and launch
        mpLoopCloser = new LoopClosing(mpMap, mpKeyFrameDatabase, mpVocabulary, mSensor!=MONOCULAR);
        mptLoopClosing = new thread(&ORB_SLAM2::LoopClosing::Run, mpLoopCloser);

        //Initialize the Viewer thread and launch
        if(bUseViewer)
        {
            mpViewer = new Viewer(this, mpFrameDrawer,mpMapDrawer,mpTracker,strSettingsFile, &this->ViewerMutex);
            mptViewer = new thread(&Viewer::Run, mpViewer);
            mpTracker->SetViewer(mpViewer);
            // mpPlotViewer = new PlotViewer(&(mpViewer->mbFinishRequested), &this->ViewerMutex, sensor, &targets);
            mpPlotViewer = new PlotViewer(this);
            mptPlotViewer = new thread(&PlotViewer::Run, mpPlotViewer);
        }

        //Set pointers between threads
        mpTracker->SetLocalMapper(mpLocalMapper);
        mpTracker->SetLoopClosing(mpLoopCloser);

        mpLocalMapper->SetTracker(mpTracker);
        mpLocalMapper->SetLoopCloser(mpLoopCloser);

        mpLoopCloser->SetTracker(mpTracker);
        mpLoopCloser->SetLocalMapper(mpLocalMapper);


    }

    cv::Mat System::TrackMonocular(const cv::Mat &im, const double &timestamp)
    {
        if(mSensor!=MONOCULAR)
        {
            cerr << "ERROR: you called TrackMonocular but input sensor was not set to Monocular." << endl;
            exit(-1);
        }
        // Check mode change
        {
            unique_lock<mutex> lock(mMutexMode);
            if(mbActivateLocalizationMode)
            {
                mpLocalMapper->RequestStop();

                // Wait until Local Mapping has effectively stopped
                while(!mpLocalMapper->isStopped())
                {
                    std::this_thread::sleep_for(chrono::milliseconds(1));
                }

                mpTracker->InformOnlyTracking(true);
                mbActivateLocalizationMode = false;
            }
            if(mbDeactivateLocalizationMode)
            {
                mpTracker->InformOnlyTracking(false);
                mpLocalMapper->Release();
                mbDeactivateLocalizationMode = false;
            }
        }

        // Check reset
        {
            unique_lock<mutex> lock(mMutexReset);
            if(mbReset)
            {
                mpTracker->Reset();
                mbReset = false;
            }
        }

        long unsigned int frameID;
        cv::Mat Tcw = mpTracker->GrabImageMonocular(im,timestamp, frameID);

        std::array<int, 2> tmp;

        mpViewer->mouseClickQueueMutex.lock();
        bool queueEmpty = mpViewer->mouseClickPoints.empty();
        if(!queueEmpty ){
            tmp = mpViewer->mouseClickPoints.front();
            mpViewer->mouseClickPoints.pop();
        }
        mpViewer->mouseClickQueueMutex.unlock();
        // cv::Mat imageUndistorted;
        // cv::undistort(im, imageUndistorted, Kcv, distCoef);
        // cv::imshow("undistort", imageUndistorted);
        // Tracking starts
        triangulatePointsMutex.lock();
        if(mTrackingState == Tracking::OK){

            cvtColor(im,currFrame,CV_RGB2GRAY);

            // Newly clicked point
            if(!queueEmpty){
                // cout << "Mouse X: " << tmp[0] << endl;
                // cout << "Mouse Y: " << tmp[1] << endl;

                int roiX = tmp[0];
                int roiY = tmp[1];

                cv::Point2f pt1,pt2;

                int roi_size = 10;

                pt1.x = roiX-roi_size;
                pt1.y = roiY-roi_size;
                pt2.x = roiX+roi_size;
                pt2.y = roiY+roi_size;

                cv::Mat mask = cv::Mat::zeros(currFrame.size(), CV_8UC1);  // type of mask is CV_8U
                cv::rectangle(mask, pt1, pt2, cv::Scalar(255));

                // cout << "im.size(): " << currFrame.size() << endl;
                // cout << "mask.size(): " << mask.size() << endl;
                // cout << "GetMatType(im): " << GetMatType(currFrame) << endl;

                // cout << "trackPoints.size 1: " << trackPoints.size() << endl;
                cv::goodFeaturesToTrack(currFrame, trackPoints, 1, 0.0001, 1, mask);
                // cout << "trackPoints.size 2: " << trackPoints.size() << endl;
                Target* t = new Target();
                t->K = K;
                targets.push_back(t);
            }
            // Continue tracking previous point
            else if(trackPoints.size() > 0){
                // cout << "Continue tracking" << endl;

                std::vector<uchar> status;
                cv::Mat error;
                std::vector< cv::Point2f > newTrackPoints;
                // cout << "prevFrame.size(): " << prevFrame.size() << endl;
                // cout << "currFrame.size(): " << currFrame.size() << endl;
                calcOpticalFlowPyrLK(prevFrame, currFrame, trackPoints, newTrackPoints, status, error);
                trackPoints = newTrackPoints;

                // Continue tracking
                if(status[0] == 1){
                    if (lastKeyFrameID != mpTracker->mpLastKeyFrame->mnId) {
                        lastKeyFrameID = mpTracker->mpLastKeyFrame->mnId;
                        Eigen::Vector2f trackPointsEig;
                        trackPointsEig << trackPoints[0].x, trackPoints[0].y;
                        targets.back()->addTriangulatePoint(trackPointsEig, mpTracker->mpLastKeyFrame );
                    }
                    mpFrameDrawer->pointThatWeTrack = trackPoints[0];
                    mpFrameDrawer->areWeTracking = true;

                }
                // Track lost
                else{
                    trackPoints.clear();
                    mpFrameDrawer->areWeTracking = false;
                }

            }
            // Nothing to track
            else{
                // cout << "Nothing to track" << endl;
            }
            for (size_t ii = 0; ii < mpPlotViewer->activeTargets.size(); ii++) {
                if(mpPlotViewer->activeTargets[ii]) {
                    targets[ii]->calculateTargetLocation();
                }
            }
        }
        // ORB slam lost track delete everything
        else{
            trackPoints.clear();
            mpFrameDrawer->areWeTracking = false;
        }
        currFrame.copyTo(prevFrame);
        // Tracking ends
        if (targets.size()) {
            targets.back()->calculateTargetLocation();
        }
        triangulatePointsMutex.unlock();

        if (frameSaving) {
            frameSaving = false;
            SaveImage(im, "frames/", timestamp);
        }
        if (isRecording) {
            SaveImage(im, "frames/", timestamp);
        }
        unique_lock<mutex> lock2(mMutexState);
        mTrackingState = mpTracker->mState;
        mTrackedMapPoints = mpTracker->mCurrentFrame.mvpMapPoints;
        mTrackedKeyPointsUn = mpTracker->mCurrentFrame.mvKeysUn;
        // cout << "Tcw matrix" << endl;
        // cout << Tcw << endl;
        prevmTrackingState = mTrackingState;
        return Tcw;
    }

    void System::ActivateLocalizationMode()
    {
        unique_lock<mutex> lock(mMutexMode);
        mbActivateLocalizationMode = true;
    }

    void System::DeactivateLocalizationMode()
    {
        unique_lock<mutex> lock(mMutexMode);
        mbDeactivateLocalizationMode = true;
    }

    bool System::MapChanged()
    {
        static int n=0;
        int curn = mpMap->GetLastBigChangeIdx();
        if(n<curn)
        {
            n=curn;
            return true;
        }
        else
            return false;
    }

    void System::Reset()
    {
        unique_lock<mutex> lock(mMutexReset);
        mbReset = true;
    }

    void System::Shutdown()
    {
        mpLocalMapper->RequestFinish();
        mpLoopCloser->RequestFinish();
        if(mpViewer)
        {
            mpViewer->RequestFinish();
            while(!mpViewer->isFinished())
                std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }

        // Wait until all thread have effectively stopped
        while(!mpLocalMapper->isFinished() || !mpLoopCloser->isFinished() || mpLoopCloser->isRunningGBA())
        {
                std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }

        if(mpViewer)
            pangolin::BindToContext("ORB-SLAM2: Map Viewer");
    }

    void System::SaveTrajectoryTUM(const string &filename)
    {
        // cout << endl << "Saving camera trajectory to " << filename << " ..." << endl;
        // if(mSensor==MONOCULAR)
        // {
        // cerr << "ERROR: SaveTrajectoryTUM cannot be used for monocular." << endl;
        // return;
        // }

        vector<KeyFrame*> vpKFs = mpMap->GetAllKeyFrames();
        if(vpKFs.empty()) {
            cout << "There is no keyframe." << endl;
            return;
        }
        sort(vpKFs.begin(),vpKFs.end(),KeyFrame::lId);

        // Transform all keyframes so that the first keyframe is at the origin.
        // After a loop closure the first keyframe might not be at the origin.
        cv::Mat Two = vpKFs[0]->GetPoseInverse();

        ofstream f;
        f.open(filename.c_str());
        f << fixed;

        // Frame pose is stored relative to its reference keyframe (which is optimized by BA and pose graph).
        // We need to get first the keyframe pose and then concatenate the relative transformation.
        // Frames not localized (tracking failure) are not saved.

        // For each frame we have a reference keyframe (lRit), the timestamp (lT) and a flag
        // which is true when tracking failed (lbL).
        list<ORB_SLAM2::KeyFrame*>::iterator lRit = mpTracker->mlpReferences.begin();
        list<double>::iterator lT = mpTracker->mlFrameTimes.begin();
        list<bool>::iterator lbL = mpTracker->mlbLost.begin();
        for(list<cv::Mat>::iterator lit=mpTracker->mlRelativeFramePoses.begin(),
                lend=mpTracker->mlRelativeFramePoses.end();lit!=lend;lit++, lRit++, lT++, lbL++)
        {
            if(*lbL)
                continue;

            KeyFrame* pKF = *lRit;

            cv::Mat Trw = cv::Mat::eye(4,4,CV_32F);

            // If the reference keyframe was culled, traverse the spanning tree to get a suitable keyframe.
            while(pKF->isBad())
            {
                Trw = Trw*pKF->mTcp;
                pKF = pKF->GetParent();
            }

            Trw = Trw*pKF->GetPose()*Two;

            cv::Mat Tcw = (*lit)*Trw;
            cv::Mat Rwc = Tcw.rowRange(0,3).colRange(0,3).t();
            cv::Mat twc = -Rwc*Tcw.rowRange(0,3).col(3);

            vector<float> q = Converter::toQuaternion(Rwc);

            f << setprecision(6) << *lT << " " <<  setprecision(9) << twc.at<float>(0) << " " << twc.at<float>(1) << " " << twc.at<float>(2) << " " << q[0] << " " << q[1] << " " << q[2] << " " << q[3] << endl;
        }
        f.close();
        //cout << endl << "trajectory saved!" << endl;
    }


    void System::SaveKeyFrameTrajectoryTUM(const string &filename)
    {
        //cout << endl << "Saving keyframe trajectory to " << filename << " ..." << endl;

        vector<KeyFrame*> vpKFs = mpMap->GetAllKeyFrames();

        if(vpKFs.empty()) {
            cout << "There is no keyframe." << endl;
            return;
        }
        sort(vpKFs.begin(),vpKFs.end(),KeyFrame::lId);

        // Transform all keyframes so that the first keyframe is at the origin.
        // After a loop closure the first keyframe might not be at the origin.
        //cv::Mat Two = vpKFs[0]->GetPoseInverse();

        ofstream f;
        f.open(filename.c_str());
        f << fixed;

        for(size_t i=0; i<vpKFs.size(); i++)
        {
            KeyFrame* pKF = vpKFs[i];

            // pKF->SetPose(pKF->GetPose()*Two);

            if(pKF->isBad())
                continue;

            cv::Mat R = pKF->GetRotation().t();
            vector<float> q = Converter::toQuaternion(R);
            cv::Mat t = pKF->GetCameraCenter();
            f << setprecision(6) << pKF->mTimeStamp << setprecision(7) << " " << t.at<float>(0) << " " << t.at<float>(1) << " " << t.at<float>(2)
                << " " << q[0] << " " << q[1] << " " << q[2] << " " << q[3] << endl;

        }

        f.close();
        //cout << endl << "trajectory saved!" << endl;
    }
    Frame System::getReferenceFrame() {
        return mpTracker->mInitialFrame;
    }

    int System::GetTrackingState()
    {
        unique_lock<mutex> lock(mMutexState);
        return mTrackingState;
    }

    vector<MapPoint*> System::GetTrackedMapPoints()
    {
        unique_lock<mutex> lock(mMutexState);
        return mTrackedMapPoints;
    }

    vector<cv::KeyPoint> System::GetTrackedKeyPointsUn()
    {
        unique_lock<mutex> lock(mMutexState);
        return mTrackedKeyPointsUn;
    }

    bool System::isPaused()
    {
        if (mpViewer)
            return mpViewer->isPaused();
        return false;
    }
    void System::SaveImage(cv::Mat im, std::string path, double timestamp) {
        stringstream filepath;
        filepath << path << std::to_string((long long)timestamp) << ".png";
        cv::imwrite(filepath.str(), im);
        filepath.str("");
    }

    void System::FindScale(size_t k[3]){
        const double eps = 1e-5;

        sensor_buffer_t* imuBuff = &(pSensor->IMUPathBuffer);

        // Get locations of KeyFrames, sort them, discard bad KFs
        // And rotate them according to reference rotation.
        sensor_buffer_t orbBuff;
        vector<KeyFrame*> allKeyFrames = mpMap->GetAllKeyFrames();

        if(allKeyFrames.empty()) {
            cout << "There is no keyframe." << endl;
            return;
        }

        sort(allKeyFrames.begin(),allKeyFrames.end(),KeyFrame::lId);

        for(size_t i = 0; i < allKeyFrames.size(); i++){
            if(!allKeyFrames[i]->isBad()){
                sensor_t tmp;
                tmp[0] = allKeyFrames[i]->mTimeStamp;
                Eigen::Vector3d camCenter;
                camCenter << (double)allKeyFrames[i]->GetCameraCenter().at<float>(0),
                             (double)allKeyFrames[i]->GetCameraCenter().at<float>(1),
                             (double)allKeyFrames[i]->GetCameraCenter().at<float>(2);
                tmp.tail<3>() = pSensor->refRot*camCenter;
                // tmp.tail<3>() = camCenter;
                orbBuff.push_back(tmp);
            }
        }

        // Do not calculate scale if there is less than or equal to 6 KF.
        if (orbBuff.size() > 6)
            orbBuff.erase(orbBuff.end() - 5, orbBuff.end());
        else return;

        // Clear scaleVector for a clean start
        scaleVector.clear();

        // ORB -> IMU mapper
        size_t* orb2imu = new size_t[orbBuff.size()];
        // Calculate indices for KFs separated by k values.
        for(size_t i = 0; i < orbBuff.size(); i++){
            if((i%k[0] == 0) || (i%k[1] == 0) || (i%k[2] == 0)){
                int findLow = pSensor->FindMatchingDataIMU(orbBuff[i][0]);
                if (findLow< 0){
                    std::cout << "Shit happened. findLow < 0. Code: " << findLow << std::endl;
                }
                else if ((size_t)findLow == imuBuff->size()) {
                    std::cout << "Requested IMU has not arrived yet. Estimation may not be accurate.\n";
                    findLow--;
                    std::cout << "Requested time: " << orbBuff[i][0] << "\n" << "Last IMU time: " << imuBuff->at(findLow) << std::endl;
                }
                orb2imu[i] = findLow;;
            }
            else{
                // You must not need these values
                orb2imu[i] = -1;
            }
        }

        double imuDiff[3] = {0};
        double orbDiff[3] = {0};
        // for: Scale Find
        //
        for(size_t i = 0; i < orbBuff.size() - 1; i++){
            sensor_t tmpScale;
            tmpScale << orbBuff[i](0), -1, -1, -1;

            for (size_t ax = 0; ax < 3; ax++) {
                if(i%k[ax] == 0 && ((i + k[ax]) < (orbBuff.size() - 1))) {
                    // Calculate IMU difference
                    imuDiff[ax] = std::abs( imuBuff->at(orb2imu[i])(ax + 1) - imuBuff->at(orb2imu[i+k[ax]])(ax + 1) );
                    // Calculate ORB difference
                    orbDiff[ax] = std::abs( orbBuff[i](ax + 1) - orbBuff[i + k[ax]](ax + 1) );

                    if(orbDiff[ax] > eps && imuDiff[ax] > eps){
                        tmpScale[ax + 1] = imuDiff[ax] / orbDiff[ax];
                    }

                    if( tmpScale[ax + 1] < eps ){
                        tmpScale[ax + 1] = (i > 0) ? scaleVector[i - 1](ax + 1) : 1;
                    }
                }
                else
                    tmpScale[ax + 1] = (i > 0) ? scaleVector[i - 1](ax + 1) : 1;
            }
            // std::cout << "Imu diff: " << imuDiff[0] << std::endl;
            // std::cout << "Orb diff: " << orbDiff[0] << std::endl;
            // std::cout << "Scale   : " << tmpScale[1] << std::endl;

            scaleVector.push_back(tmpScale);
        }

        // We are done with orb2imu.
        delete[] orb2imu;
        // end for: Scale find

        // for (auto k: scaleVector) {
            // cout << "Not filtered: " <<  k << endl;
        // }

        /* Filter scaleVector */

        // If there is less than 10 scale do not filter
        if(scaleVector.size() >= 10) {

            // Calc mean
            double meanV[3] = {0};
            for(size_t i = 0; i < scaleVector.size(); i++){
                for (size_t ax = 0; ax < 3; ax++) {
                    if(i%k[ax] == 0){
                        meanV[ax] += scaleVector[i](1);
                    }
                }
            }

            meanV[0] = meanV[0] / scaleVector.size() * k[0];
            meanV[1] = meanV[1] / scaleVector.size() * k[1];
            meanV[2] = meanV[2] / scaleVector.size() * k[2];

            // Calc standard deviation
            double stdV[3] = {0};
            for(size_t i = 0; i < scaleVector.size(); i++){
                for (size_t ax = 0; i < 3; i++) {
                    if(i%k[ax]){
                        stdV[ax] += std::pow( scaleVector[i](1) - meanV[ax], 2 ) / scaleVector.size() / k[ax];
                    }
                }
            }
            stdV[0] = std::sqrt(stdV[0] / scaleVector.size() * k[0]);
            stdV[1] = std::sqrt(stdV[1] / scaleVector.size() * k[1]);
            stdV[2] = std::sqrt(stdV[2] / scaleVector.size() * k[2]);

            // Apply filter
            for(size_t i = 0; i < scaleVector.size(); i++){
                for (size_t ax = 0; ax < 3; ax++) {
                    if( (meanV[ax] - scaleVector[i](ax + 1)) > stdV[ax] ){
                        scaleVector[i](ax + 1) = meanV[ax] - stdV[ax];
                    }
                    else if( (scaleVector[i](ax + 1) - meanV[ax]) > stdV[ax] ){
                        scaleVector[i](ax + 1) = meanV[ax] + stdV[ax];
                    }
                }
            }

            // for (auto k: scaleVector) {
                // cout << "Filtered: " <<  k << endl;
            // }
        }

        ApplyScale();
    }

    void System::ApplyScale() {

        //      ----------------------------GET UPDATED ALL FRAME------------------------------

        if(scaleVector.size() < 1){
            std::cout << "There is only one scale" << std::endl;
            return;
        }
        sensor_buffer_t allOrbBuff;

        cv::Mat Two = mpMap->GetAllKeyFrames()[0]->GetPoseInverse();
        list<ORB_SLAM2::KeyFrame*>::iterator lRit = mpTracker->mlpReferences.begin();
        list<double>::iterator lT = mpTracker->mlFrameTimes.begin();
        list<bool>::iterator lbL = mpTracker->mlbLost.begin();
        int all_size = 0;
        for(list<cv::Mat>::iterator lit=mpTracker->mlRelativeFramePoses.begin(),
        lend=mpTracker->mlRelativeFramePoses.end();lit!=lend;lit++, lRit++, lT++, lbL++)
        {
            if(*lbL) continue;

            KeyFrame* pKF = *lRit;
            cv::Mat Trw = cv::Mat::eye(4,4,CV_32F);
            while(pKF->isBad())
            {
                Trw = Trw*pKF->mTcp;
                pKF = pKF->GetParent();
            }

            Trw = Trw*pKF->GetPose()*Two;

            cv::Mat Tcw = (*lit)*Trw;
            cv::Mat Rwc = Tcw.rowRange(0,3).colRange(0,3).t();
            cv::Mat twc = -Rwc*Tcw.rowRange(0,3).col(3);

            sensor_t tmp;
            tmp(0) = *lT;
            tmp(1) = (double)twc.at<float>(0);
            tmp(2) = (double)twc.at<float>(1);
            tmp(3) = (double)twc.at<float>(2);

            tmp.tail<3>() = pSensor->refRot*tmp.tail<3>();

            allOrbBuff.push_back(tmp);
            all_size++;
        }

        scaledOrbPath.clear();

        size_t frameIdx = 0;
        size_t targetIdx = 0;

        for(size_t i = 1; i < scaleVector.size(); i++){
            while( allOrbBuff[frameIdx](0) < scaleVector[i](0) ){
                sensor_t tmp;
                tmp(0) = allOrbBuff[frameIdx](0);
                tmp(1) = allOrbBuff[frameIdx](1) * scaleVector[i-1](1);
                tmp(2) = allOrbBuff[frameIdx](2) * scaleVector[i-1](2);
                tmp(3) = allOrbBuff[frameIdx](3) * scaleVector[i-1](3);
                // tmp(1) = allOrbBuff[frameIdx](1) *100;
                // tmp(2) = allOrbBuff[frameIdx](2) *100;
                // tmp(3) = allOrbBuff[frameIdx](3) *100;
                scaledOrbPath.push_back(tmp);
                frameIdx++;
                // std::cout << "-------------KF No: " << i << " --------------\n";
                // std::cout << "all orb buffer: " << allOrbBuff[frameIdx](1) << "\n";
                // std::cout << "scale         : " << scaleVector[i-1](1) << "\n";
                // std::cout << "scaled buffer : " << tmp(1) << "\n";
            }
       	    while( (targets[targetIdx]->firstTime < scaleVector[i](0)) && (targetIdx < targets.size()) ){
                targets[targetIdx]->scale[0] = scaleVector[i-1](1);
                targets[targetIdx]->scale[1] = scaleVector[i-1](2);
                targets[targetIdx]->scale[2] = scaleVector[i-1](3);
                targetIdx++;
            }
        }
        while(frameIdx < allOrbBuff.size()){
            sensor_t tmp;
            tmp(0) = allOrbBuff[frameIdx](0);
            tmp(1) = allOrbBuff[frameIdx](1) * scaleVector.back()(1);
            tmp(2) = allOrbBuff[frameIdx](2) * scaleVector.back()(2);
            tmp(3) = allOrbBuff[frameIdx](3) * scaleVector.back()(3);
            // tmp(1) = allOrbBuff[frameIdx](1) * 100;
            // tmp(2) = allOrbBuff[frameIdx](2) * 100;
            // tmp(3) = allOrbBuff[frameIdx](3) * 100;
            scaledOrbPath.push_back(tmp);
            frameIdx++;
        }
        while( targetIdx < targets.size() ){
            targets[targetIdx]->scale[0] = scaleVector.back()(1);
            targets[targetIdx]->scale[1] = scaleVector.back()(2);
            targets[targetIdx]->scale[2] = scaleVector.back()(3);
            targetIdx++;
        }
        // for (auto k: scaledOrbPath) {
            // cout << "Scaled: " <<  k << endl;
        // }
    }

} //namespace ORB_SLAM
