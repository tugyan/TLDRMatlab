
#include "math.h"
#include <Eigen/Dense>
#include "GPSConvert.h"

using namespace geodetic_converter;


// Geodetic system parameters
static long double kSemimajorAxis = 6378137;
//static double kSemiminorAxis = 6356752.3142;
static long double kSemiminorAxis = 6356752.31424518;
//static double kFirstEccentricitySquared = 6.69437999014 * 0.001;
static long double kFirstEccentricitySquared = 6.69437999014132 * 0.001;
//static double kSecondEccentricitySquared = 6.73949674228 * 0.001;
static long double kSecondEccentricitySquared = 6.73949674227647 * 0.001;

static long double kFlattening = 1 / 298.257223563;

  void GeodeticConverter::getReference(long double* latitude, long double* longitude, long double* altitude)
  {
    *latitude = initial_latitude_;
    *longitude = initial_longitude_;
    *altitude = initial_altitude_;
  }

  void GeodeticConverter::initialiseReference(const long double latitude, const long double longitude, const long double altitude)
  {
    // Save NED origin
    initial_latitude_ = deg2Rad(latitude);
    initial_longitude_ = deg2Rad(longitude);
    initial_altitude_ = altitude;

    // Compute ECEF of NED origin
    geodetic2Ecef(latitude, longitude, altitude, &initial_ecef_x_, &initial_ecef_y_, &initial_ecef_z_);

    // Compute ECEF to NED and NED to ECEF matrices
    long double phiP = atan2(initial_ecef_z_, sqrt(pow(initial_ecef_x_, 2) + pow(initial_ecef_y_, 2)));

    ecef_to_ned_matrix_ = nRe(phiP, initial_longitude_);
    ned_to_ecef_matrix_ = nRe(initial_latitude_, initial_longitude_).transpose();

    haveReference_ = true;
  }

  void GeodeticConverter::geodetic2Ecef(const long double latitude, const long double longitude, const long double altitude, long double* x,
		  long double* y, long double* z)
  {
    // Convert geodetic coordinates to ECEF.
    // http://code.google.com/p/pysatel/source/browse/trunk/coord.py?r=22
	  long double lat_rad = deg2Rad(latitude);
	  long double lon_rad = deg2Rad(longitude);
	  long double xi = sqrt(1 - kFirstEccentricitySquared * sin(lat_rad) * sin(lat_rad));
    *x = (kSemimajorAxis / xi + altitude) * cos(lat_rad) * cos(lon_rad);
    *y = (kSemimajorAxis / xi + altitude) * cos(lat_rad) * sin(lon_rad);
    *z = (kSemimajorAxis / xi * (1 - kFirstEccentricitySquared) + altitude) * sin(lat_rad);
  }

  void GeodeticConverter::ecef2Geodetic(const long double x, const long double y, const long double z, long double* latitude,
		  long double* longitude, long double* altitude)
  {
    // Convert ECEF coordinates to geodetic coordinates.
    // J. Zhu, "Conversion of Earth-centered Earth-fixed coordinates
    // to geodetic coordinates," IEEE Transactions on Aerospace and
    // Electronic Systems, vol. 30, pp. 957-961, 1994.

	  long double r = sqrt(x * x + y * y);
	  long double Esq = kSemimajorAxis * kSemimajorAxis - kSemiminorAxis * kSemiminorAxis;
	  long double F = 54 * kSemiminorAxis * kSemiminorAxis * z * z;
	  long double G = r * r + (1 - kFirstEccentricitySquared) * z * z - kFirstEccentricitySquared * Esq;
	  long double C = (kFirstEccentricitySquared * kFirstEccentricitySquared * F * r * r) / pow(G, 3);
	  long double S = cbrt(1 + C + sqrt(C * C + 2 * C));
	  long double P = F / (3 * pow((S + 1 / S + 1), 2) * G * G);
	  long double Q = sqrt(1 + 2 * kFirstEccentricitySquared * kFirstEccentricitySquared * P);
	  long double r_0 = -(P * kFirstEccentricitySquared * r) / (1 + Q)
        + sqrt(
            0.5 * kSemimajorAxis * kSemimajorAxis * (1 + 1.0 / Q)
                - P * (1 - kFirstEccentricitySquared) * z * z / (Q * (1 + Q)) - 0.5 * P * r * r);
	  long double U = sqrt(pow((r - kFirstEccentricitySquared * r_0), 2) + z * z);
	  long double V = sqrt(
        pow((r - kFirstEccentricitySquared * r_0), 2) + (1 - kFirstEccentricitySquared) * z * z);
	  long double Z_0 = kSemiminorAxis * kSemiminorAxis * z / (kSemimajorAxis * V);
    *altitude = U * (1 - kSemiminorAxis * kSemiminorAxis / (kSemimajorAxis * V));
    *latitude = rad2Deg(atan((z + kSecondEccentricitySquared * Z_0) / r));
    *longitude = rad2Deg(atan2(y, x));
  }

  void GeodeticConverter::ecef2Ned(const long double x, const long double y, const long double z, long double* north, long double* east,
		  long double* down)
  {
    // Converts ECEF coordinate position into local-tangent-plane NED.
    // Coordinates relative to given ECEF coordinate frame.

    Eigen::Vector3d vect, ret;
    vect(0) = x - initial_ecef_x_;
    vect(1) = y - initial_ecef_y_;
    vect(2) = z - initial_ecef_z_;
    ret = ecef_to_ned_matrix_ * vect;
    *north = ret(0);
    *east = ret(1);
    *down = -ret(2);
  }

  void GeodeticConverter::ned2Ecef(const long double north, const long double east, const long double down, long double* x, long double* y,
		  long double* z)
  {
    // NED (north/east/down) to ECEF coordinates
    Eigen::Vector3d ned, ret;
    ned(0) = north;
    ned(1) = east;
    ned(2) = -down;
    ret = ned_to_ecef_matrix_ * ned;
    *x = ret(0) + initial_ecef_x_;
    *y = ret(1) + initial_ecef_y_;
    *z = ret(2) + initial_ecef_z_;
  }

  void GeodeticConverter::geodetic2Ned(const long double latitude, const long double longitude, const long double altitude,
		  long double* north, long double* east, long double* down)
  {
    // Geodetic position to local NED frame
	  long double x, y, z;
    geodetic2Ecef(latitude, longitude, altitude, &x, &y, &z);
    ecef2Ned(x, y, z, north, east, down);
  }

  void GeodeticConverter::ned2Geodetic(const long double north, const long double east, const long double down, long double* latitude,
		  long double* longitude, long double* altitude)
  {
    // Local NED position to geodetic coordinates
	  long double x, y, z;
    ned2Ecef(north, east, down, &x, &y, &z);
    ecef2Geodetic(x, y, z, latitude, longitude, altitude);
  }

  void GeodeticConverter::geodetic2Enu(const long double latitude, const long double longitude, const long double altitude,
		  long double* east, long double* north, long double* up)
  {
    // Geodetic position to local ENU frame
	  long double x, y, z;
    geodetic2Ecef(latitude, longitude, altitude, &x, &y, &z);

    long double aux_north, aux_east, aux_down;
    ecef2Ned(x, y, z, &aux_north, &aux_east, &aux_down);

    *east = aux_east;
    *north = aux_north;
    *up = -aux_down;
  }

  void GeodeticConverter::enu2Geodetic(const long double east, const long double north, const long double up, long double* latitude,
		  long double* longitude, long double* altitude)
  {
    // Local ENU position to geodetic coordinates

    const long double aux_north = north;
    const long double aux_east = east;
    const long double aux_down = -up;
    long double x, y, z;
    ned2Ecef(aux_north, aux_east, aux_down, &x, &y, &z);
    ecef2Geodetic(x, y, z, latitude, longitude, altitude);
  }

  inline Eigen::Matrix3d GeodeticConverter::nRe(const long double lat_radians, const long double lon_radians)
  {
    const long double sLat = sin(lat_radians);
    const long double sLon = sin(lon_radians);
    const long double cLat = cos(lat_radians);
    const long double cLon = cos(lon_radians);

    Eigen::Matrix3d ret;
    ret(0, 0) = -sLat * cLon;
    ret(0, 1) = -sLat * sLon;
    ret(0, 2) = cLat;
    ret(1, 0) = -sLon;
    ret(1, 1) = cLon;
    ret(1, 2) = 0.0;
    ret(2, 0) = cLat * cLon;
    ret(2, 1) = cLat * sLon;
    ret(2, 2) = sLat;

    return ret;
  }

  inline
  long double GeodeticConverter::rad2Deg(const long double radians)
  {
    return (radians / M_PI) * 180.0;
  }

  inline
  long double GeodeticConverter::deg2Rad(const long double degrees)
  {
    return (degrees / 180.0) * M_PI;
  }
