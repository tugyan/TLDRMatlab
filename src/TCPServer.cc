#include "TCPServer.h"

tcp_server::tcp_server(asio::io_service& io_service, Sensor *sns, bool *conAv):
        acceptor_(io_service, tcp::endpoint(tcp::v4(), 5275)) {

    connectionEstablised = conAv;
    sensor = sns;
    start_accept();
}

void tcp_server::start_accept()
{
    tcp_connection::pointer new_connection = tcp_connection::create(acceptor_.get_io_service());
    // acceptor_.accept(new_connection->socket(), &new_connection);
    acceptor_.async_accept(new_connection->socket(), std::bind(&tcp_server::handle_accept, this, new_connection, std::placeholders::_1));
}

void tcp_server::handle_accept(tcp_connection::pointer new_connection,
  const asio::error_code& error)
{
    if (!error)
    {
        std::cout << "Connected" << std::endl;
        *connectionEstablised = true;
        new_connection->setSensor(sensor);
        new_connection->start();

        curr_connection = new_connection;
    }

    start_accept();
}

sensor_t tcp_server::requestGimbal(double ts) {
    return curr_connection->requestGimbal(ts);
}
