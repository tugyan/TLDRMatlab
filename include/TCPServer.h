// #include <asio.hpp>
#include "TCPConnection.h"

class tcp_server {
public:
    tcp_server(asio::io_service& io_service, Sensor*, bool*);
    sensor_t requestGimbal(double);
private:
    void start_accept();
    bool *connectionEstablised;
    void handle_accept(tcp_connection::pointer new_connection,
            const asio::error_code& error);
    Sensor* sensor;
    tcp::acceptor acceptor_;
    tcp_connection::pointer curr_connection;
};
