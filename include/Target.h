#ifndef TARGET_H
#define TARGET_H

#include "KeyFrame.h"
#include<Eigen/Core>
#include<opencv2/core/core.hpp>

using namespace ORB_SLAM2;


class Target
{
public:
    std::vector<Eigen::Vector2f> triangulatePixels;
    std::vector<KeyFrame*> triangulateKeyFrames;

    double gpsX;
    double gpsY;
    double firstTime;
    double lastTime;
    Eigen::Matrix3f K;
    Eigen::Vector3f location;
    double meanError;
    bool isLocated = false;

    std::array<double, 3> scale;

    void addTriangulatePoint(Eigen::Vector2f, KeyFrame*);
    void calculateTargetLocation();
};
#endif
