/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PLOTVIEWER_H
#define PLOTVIEWER_H

#include <pangolin/pangolin.h>
#include "Sensor.h"
#include "System.h"
#include "Target.h"
#include <thread>
#include <mutex>

namespace ORB_SLAM2 {
    class System;
class MapHandler: public pangolin::Handler {

public:
void MouseMotion( pangolin::View& /* d */,
                    int  	x,
                    int  	y,
                    int  	button_state) {

    if(button_state == pangolin::MouseButtonLeft) {
        if (isPressed) {
            isPressed = false;
            prevx = x;
            prevy = y;
        }
        else {
            dx = (prevx - x);
            dy = (prevy - y);
            // std::cout << "x: " << dx << " y: " << dy << std::endl;
            dir_x = (0 < dx) - (dx < 0);
            dir_y = (0 < dy) - (dy < 0);
            prevx = x;
            prevy = y;

        }
    }
}
void Mouse( pangolin::View& //d
        , pangolin::MouseButton button
        , int //x
        , int //y
        , bool pressed,
        int //button_state
        ) {
    isPressed = pressed;
    if (button == pangolin::MouseWheelUp) {
        if (zoom_factor > 0.1)
            zoom_factor -= 0.1;
    }
    else if (button == pangolin::MouseWheelDown) {
        zoom_factor += 0.1;
    }
}
bool isPressed = false;
int dx = 0;
int dy = 0;
int dir_x = 0;
int dir_y = 0;
float zoom_factor = 1;

private:
int prevx = 0;
int prevy = 0;

};
class PlotViewer
{
public:
    PlotViewer(bool*, std::mutex*, Sensor* sensor, std::vector<Target*>*);
    PlotViewer(System*);
    void Run();
    std::vector<bool> activeTargets;

private:
    Eigen::Vector2d GPSToPixel(double latitude, double longitude) const;
    Eigen::Vector2d MetersToPixel(float x, float y) const;
    Eigen::Vector2d PixelToMeters(Eigen::Vector2d pxs) const;
    Eigen::Vector2d GPSToGL(double latitude, double longitude) const;
    Eigen::Vector2d GLToGPS(double x, double y) const;
    Eigen::Vector2d MetersToGL(float x, float y) const;
    Eigen::Vector2d StringToGPS(std::string) const;

    void DrawGPSRoute();
    void DrawEstimateRoute();
    void DrawTargets();

    std::mutex* ViewerMutex;
    bool* shutDown;
    Sensor* sensor;
    std::vector<Target*>* pTargets;
    std::vector<pangolin::Var<bool>> targetBoxes;
    std::vector<std::array<pangolin::Var<std::string>, 3>> posTargets;

    System* pSystem;
    sensor_buffer_t* ORBEstimates;

    MapHandler* hdl;

    Eigen::Vector2d initPoint;

    // Data logger object
    pangolin::DataLog altitudeLog;

    float roi_l, roi_b;
    const float ROI_SIZE_X = 640.0f;
    const float ROI_SIZE_Y = 640.0f;
    const float MAP_SIZE_X = 4*ROI_SIZE_X;
    const float MAP_SIZE_Y = 5*ROI_SIZE_X;

    ////////////////////////////
    // ----> +LONGITUDE
    //  |
    //  |
    //  |
    //  v
    // -LATITUDE
    //

    const double LONGITUDE_PER_PIXEL = 5.3484375e-06;
    const double LATITUDE_PER_PIXEL = 4.1328125e-06;
    const double MAP_TOP_LEFT_LATITUDE = 39.8771195;
    const double MAP_TOP_LEFT_LONGITUDE = 32.744873;
    const float METERS_PER_PIXEL = 0.457713966;
};
}

#endif // PLOTVIEWER_H
