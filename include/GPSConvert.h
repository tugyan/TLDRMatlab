#ifndef GPSCONVERT_H_
#define GPSCONVERT_H_

#include "math.h"
#include <Eigen/Dense>

namespace geodetic_converter {

class GeodeticConverter
{
 public:

  GeodeticConverter() {haveReference_= false; }

  bool isInitialised() {return haveReference_;}

  void getReference(long double*, long double*, long double*);

  void initialiseReference(const long double, const long double, const long double);

  void geodetic2Ecef(const long double, const long double, const long double, long double*,
		  long double*, long double*);

  void ecef2Geodetic(const long double, const long double, const long double, long double*,
		  long double*, long double*);

  void ecef2Ned(const long double, const long double, const long double, long double*, long double*,
		  long double*);

  void ned2Ecef(const long double, const long double, const long double, long double*, long double*,
		  long double*);

  void geodetic2Ned(const long double, const long double, const long double,
		  long double*, long double*, long double*);

  void ned2Geodetic(const long double, const long double, const long double, long double*,
		  long double*, long double*);

  void geodetic2Enu(const long double, const long double, const long double,
		  long double*, long double*, long double*);

  void enu2Geodetic(const long double, const long double, const long double, long double*,
		  long double*, long double*);

 private:
  inline Eigen::Matrix3d nRe(const long double, const long double);

  inline
  long double rad2Deg(const long double);

  inline
  long double deg2Rad(const long double);

  long double initial_latitude_;
  long double initial_longitude_;
  long double initial_altitude_;

  long double initial_ecef_x_;
  long double initial_ecef_y_;
  long double initial_ecef_z_;

  Eigen::Matrix3d ecef_to_ned_matrix_;
  Eigen::Matrix3d ned_to_ecef_matrix_;

  bool haveReference_;

}; // class GeodeticConverter
}; // namespace geodetic_converter

#endif // GPSCONVERT_H_
