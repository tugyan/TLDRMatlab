#ifndef SENSOR_H
#define SENSOR_H

#include"IMU.h"
#include<iostream>
#include<mutex>
#include<exception>
#include<cmath>
#include<opencv2/core/core.hpp>
#include"Utils.h"
#include"GPSConvert.h"
#include"KeyFrame.h"
#include <Eigen/StdVector>

typedef Eigen::Vector4d sensor_t;
typedef std::vector<sensor_t, Eigen::aligned_allocator<sensor_t>> sensor_buffer_t;

class Sensor {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        Sensor();
        int FindMatchingDataIMU(double timestamp);
        void pushData(IMUData);
        void integrate();
        double findIMUDistance(double fromTs, double tillTs);
        double calculateScale(ORB_SLAM2::KeyFrame*, ORB_SLAM2::KeyFrame*);
        int findTimeIMUPath(double ts);
        void convertGPS();
        void SetRotationMatrix(sensor_t gb);
        void SetORBInitialized(double iTime) {
            isORBInitialized = true;
            initORBTime = iTime;
        }
        double currScale;
        double initTime;

        bool shutdownRequested;

        std::mutex SensorMutex;
        std::mutex IntegrationMutex;
        std::mutex GPSMutex;

        std::vector<IMUData> IMUDataBuffer;

        sensor_buffer_t IMUPathBuffer;
        sensor_buffer_t GPSPathBuffer;

        sensor_buffer_t* ORBMatlabBuffer = nullptr;
        sensor_buffer_t* TargetMatlabBuffer = nullptr;

        geodetic_converter::GeodeticConverter GPSCon;

        // Initial coordinates. Corresponds to near EB building.
	long double lat0 = 39.8720295;
        long double lon0 = 32.7501075;
        long double alt0 = 0;
        bool isGPSInitialiased = false;

        bool isORBInitialized = false;
        double initORBTime;

        Eigen::Matrix3d refRot;
        Eigen::Matrix3d coorRot;


    private:


        double diffIMUx;
        double diffIMUy;
        double diffIMUz;
};

#endif
