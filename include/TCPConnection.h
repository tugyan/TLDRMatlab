#include <asio.hpp>
#include <iostream>
#include <fstream>
#include <queue>
#include <thread>
#include "Sensor.h"
#include "Utils.h"

using asio::ip::tcp;


class tcp_connection : public std::enable_shared_from_this<tcp_connection>
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    typedef std::shared_ptr<tcp_connection> pointer;

    static pointer create(asio::io_service& io_service)
        { return pointer(new tcp_connection(io_service)); }

    tcp::socket& socket(){ return socket_;}
    void start();
    void setSensor(Sensor *sns) {this->sensor = sns;}

    sensor_t requestGimbal(double);
private:
    tcp_connection(asio::io_service& io_service) : socket_(io_service){}
    void start_read();
    void handle_write(const asio::error_code& ec);
    void handle_read(const asio::error_code& ec);

    bool gimbalAvailable;
    sensor_t gData;
    std::mutex receiveMutex;
    bool isLastPackIMU;
    std::string strGimBuf;
    std::string strIMUBuf;
    std::ofstream fIMU;
    std::ofstream fGimball;
    tcp::socket socket_;
    std::string message_;
    asio::streambuf input_buffer_;
    Sensor *sensor;
};
