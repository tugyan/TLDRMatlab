#ifndef IMU_H
#define IMU_H
#include<sstream>
#include<mutex>
#include<iostream>
#include<string>

class IMUData {
    public:
        IMUData():v_x(0), v_y(0), v_z(0), alt(0) ,lat(0), longt(0){};
        double timestamp;
        float  v_x, v_y, v_z, alt;
        long double lat, longt;

        void FillIMU(std::string);
        bool isGPSAvailable() { return isGPS; }
        bool FillData(std::string);
        void setGPSAvailable(bool gps) { isGPS = gps; }
        void printData();

        bool isValid = false;

        bool operator<(const double rhs) const {
            return this->timestamp < rhs;
        }

        bool operator>(const double rhs) const {
            return this->timestamp > rhs;
        }

        bool operator==(const double rhs) const {
            return this->timestamp == rhs;
        }
    private:
        bool isGPS = false;

};

#endif
